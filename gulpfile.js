var gulp = require('gulp');
var concat = require('gulp-concat');
var path = {
    css_base_path: 'src/assets/css/',
    css_super_admin_path: 'super-admin/',
    js_base_path: 'src/assets/js/',
    js_super_admin_path: 'super-admin/',
    css_level_2_path: 'level-2/',
    js_level_2_path: 'level-2/',
    css_level_3_path: 'level-3/',
    js_level_3_path: 'level-3/',
    compile_base_path: 'src/assets/build/'
};

//combine super admin js
gulp.task('all-scripts', function () {
    return gulp.src([
        path.js_base_path + 'core/vendors.bundle.js',
        path.js_base_path + path.js_super_admin_path + 'scripts.bundle.js',
        path.js_base_path + 'plugin/toastr.js',
        path.js_base_path + 'plugin/charts/amcharts.js',
        path.js_base_path + 'plugin/charts/pie.js',
        path.js_base_path + 'plugin/charts/serial.js',
        path.js_base_path + 'plugin/charts/ammap.js',
        // path.js_base_path + 'plugin/fullcalendar/fullcalendar.bundle.js',
        path.js_base_path + path.js_super_admin_path + 'dashboard.js'
    ])
        .pipe(concat('all.js'))
        .pipe(gulp.dest(path.compile_base_path + 'js'))
});

//combine super admin css
gulp.task('level-1-sheets', function () {
    return gulp.src([
        path.css_base_path + 'core/vendors.bundle.css',
        // path.css_base_path + 'plugin/fullcalendar/fullcalendar.bundle.css',
        path.css_base_path + path.css_super_admin_path + 'style.bundle.css'
    ])
        .pipe(concat('level-1.css'))
        .pipe(gulp.dest(path.compile_base_path + 'css'))
});
//combine level-2 css
gulp.task('level-2-sheets', function () {
    return gulp.src([
        path.css_base_path + 'core/vendors.bundle.css',
        // path.css_base_path + 'plugin/fullcalendar/fullcalendar.bundle.css',
        path.css_base_path + path.css_level_2_path + 'style.bundle.css'
    ])
        .pipe(concat('level-2.css'))
        .pipe(gulp.dest(path.compile_base_path + 'css'))
});

//combine level-3 css
gulp.task('level-3-sheets', function () {
    return gulp.src([
        path.css_base_path + 'core/vendors.bundle.css',
        path.css_base_path + path.css_level_3_path + 'style.bundle.css'
    ])
        .pipe(concat('level-3.css'))
        .pipe(gulp.dest(path.compile_base_path + 'css'))
});

//gulp default
gulp.task('default', [
    'all-scripts',
    'level-1-sheets',
    'level-2-sheets',
    'level-3-sheets'
]);