//== Daily Sales chart.
function dailySales(daily_sale) {
    console.log(daily_sale);
    var labels = [];
    var data = [];
    $.each(daily_sale, function (key, value) {
        var label_date = moment(value.format_create_date).format('DD MMMM YY');
        labels.push(label_date);
        data.push(value.lead_counter)
    });
    var chartData = {
        labels: labels,
        datasets: [{
            //label: 'Dataset 1',
            backgroundColor: mUtil.getColor('success'),
            data: data
        }, {
            backgroundColor: '#f3f3fb',
            data: data
        }]
    };
    var chartContainer = $('#m_chart_daily_sales');
    if (chartContainer.length == 0) {
        return;
    }
    var chart = new Chart(chartContainer, {
        type: 'bar',
        data: chartData,
        options: {
            title: {
                display: false,
            },
            tooltips: {
                intersect: false,
                mode: 'nearest',
                xPadding: 10,
                yPadding: 10,
                caretPadding: 10
            },
            legend: {
                display: false
            },
            responsive: true,
            maintainAspectRatio: false,
            barRadius: 4,
            scales: {
                xAxes: [{
                    display: false,
                    gridLines: false,
                    stacked: true
                }],
                yAxes: [{
                    display: false,
                    stacked: true,
                    gridLines: false
                }]
            },
            layout: {
                padding: {
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0
                }
            }
        }
    });
}

//== profit chart
function ProfitChart(client, sales, leads) {
    if ($('#m_chart_profit_share').length == 0) {
        return;
    }
    var chart = new Chartist.Pie('#m_chart_profit_share', {
        series: [{
            value: leads,
            className: 'custom',
            meta: {
                color: mUtil.getColor('brand')
            }
        },
            {
                value: sales,
                className: 'custom',
                meta: {
                    color: mUtil.getColor('accent')
                }
            },
            {
                value: client,
                className: 'custom',
                meta: {
                    color: mUtil.getColor('warning')
                }
            }
        ],
        labels: [1, 2, 3]
    }, {
        donut: true,
        donutWidth: 17,
        showLabel: false
    });

    chart.on('draw', function (data) {
        if (data.type === 'slice') {
            // Get the total path length in order to use for dash array animation
            var pathLength = data.element._node.getTotalLength();

            // Set a dasharray that matches the path length as prerequisite to animate dashoffset
            data.element.attr({
                'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
            });

            // Create animation definition while also assigning an ID to the animation for later sync usage
            var animationDefinition = {
                'stroke-dashoffset': {
                    id: 'anim' + data.index,
                    dur: 1000,
                    from: -pathLength + 'px',
                    to: '0px',
                    easing: Chartist.Svg.Easing.easeOutQuint,
                    // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
                    fill: 'freeze',
                    'stroke': data.meta.color
                }
            };

            // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
            if (data.index !== 0) {
                animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
            }

            // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us

            data.element.attr({
                'stroke-dashoffset': -pathLength + 'px',
                'stroke': data.meta.color
            });

            // We can't use guided mode as the animations need to rely on setting begin manually
            // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
            data.element.animate(animationDefinition, false);
        }
    });

    // For the sake of the example we update the chart every time it's created with a delay of 8 seconds
    chart.on('created', function () {
        if (window.__anim21278907124) {
            clearTimeout(window.__anim21278907124);
            window.__anim21278907124 = null;
        }
        window.__anim21278907124 = setTimeout(chart.update.bind(chart), 15000);
    });
}

//== event init
function EventInit() {
if ($('#m_calendar').length === 0) {
    return;
}
var todayDate = moment().startOf('day');
var YM = todayDate.format('YYYY-MM');
var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
var TODAY = todayDate.format('YYYY-MM-DD');
var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

$('#m_calendar').fullCalendar({
    header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay,listWeek'
    },
    editable: true,
    eventLimit: true, // allow "more" link when too many events
    navLinks: true,
    defaultDate: moment('2017-09-15'),
    events: [
        {
            title: 'Meeting',
            start: moment('2017-08-28'),
            description: 'Lorem ipsum dolor sit incid idunt ut',
            className: "m-fc-event--light m-fc-event--solid-warning"
        },
        {
            title: 'Conference',
            description: 'Lorem ipsum dolor incid idunt ut labore',
            start: moment('2017-08-29T13:30:00'),
            end: moment('2017-08-29T17:30:00'),
            className: "m-fc-event--accent"
        },
        {
            title: 'Dinner',
            start: moment('2017-08-30'),
            description: 'Lorem ipsum dolor sit tempor incid',
            className: "m-fc-event--light  m-fc-event--solid-danger"
        },
        {
            title: 'All Day Event',
            start: moment('2017-09-01'),
            description: 'Lorem ipsum dolor sit incid idunt ut',
            className: "m-fc-event--danger m-fc-event--solid-focus"
        },
        {
            title: 'Reporting',
            description: 'Lorem ipsum dolor incid idunt ut labore',
            start: moment('2017-09-03T13:30:00'),
            end: moment('2017-09-04T17:30:00'),
            className: "m-fc-event--accent"
        },
        {
            title: 'Company Trip',
            start: moment('2017-09-05'),
            end: moment('2017-09-07'),
            description: 'Lorem ipsum dolor sit tempor incid',
            className: "m-fc-event--primary"
        },
        {
            title: 'ICT Expo 2017 - Product Release',
            start: moment('2017-09-09'),
            description: 'Lorem ipsum dolor sit tempor inci',
            className: "m-fc-event--light m-fc-event--solid-primary"
        },
        {
            title: 'Dinner',
            start: moment('2017-09-12'),
            description: 'Lorem ipsum dolor sit amet, conse ctetur'
        },
        {
            id: 999,
            title: 'Repeating Event',
            start: moment('2017-09-15T16:00:00'),
            description: 'Lorem ipsum dolor sit ncididunt ut labore',
            className: "m-fc-event--danger"
        },
        {
            id: 1000,
            title: 'Repeating Event',
            description: 'Lorem ipsum dolor sit amet, labore',
            start: moment('2017-09-18T19:00:00'),
        },
        {
            title: 'Conference',
            start: moment('2017-09-20T13:00:00'),
            end: moment('2017-09-21T19:00:00'),
            description: 'Lorem ipsum dolor eius mod tempor labore',
            className: "m-fc-event--accent"
        },
        {
            title: 'Meeting',
            start: moment('2017-09-11'),
            description: 'Lorem ipsum dolor eiu idunt ut labore'
        },
        {
            title: 'Lunch',
            start: moment('2017-09-18'),
            className: "m-fc-event--info m-fc-event--solid-accent",
            description: 'Lorem ipsum dolor sit amet, ut labore'
        },
        {
            title: 'Meeting',
            start: moment('2017-09-24'),
            className: "m-fc-event--warning",
            description: 'Lorem ipsum conse ctetur adipi scing'
        },
        {
            title: 'Happy Hour',
            start: moment('2017-09-24'),
            className: "m-fc-event--light m-fc-event--solid-focus",
            description: 'Lorem ipsum dolor sit amet, conse ctetur'
        },
        {
            title: 'Dinner',
            start: moment('2017-09-24'),
            className: "m-fc-event--solid-focus m-fc-event--light",
            description: 'Lorem ipsum dolor sit ctetur adipi scing'
        },
        {
            title: 'Birthday Party',
            start: moment('2017-09-24'),
            className: "m-fc-event--primary",
            description: 'Lorem ipsum dolor sit amet, scing'
        },
        {
            title: 'Company Event',
            start: moment('2017-09-24'),
            className: "m-fc-event--danger",
            description: 'Lorem ipsum dolor sit amet, scing'
        },
        {
            title: 'Click for Google',
            url: 'http://google.com/',
            start: moment('2017-09-26'),
            className: "m-fc-event--solid-info m-fc-event--light",
            description: 'Lorem ipsum dolor sit amet, labore'
        }
    ],

    eventRender: function (event, element) {
        if (element.hasClass('fc-day-grid-event')) {
            element.data('content', event.description);
            element.data('placement', 'top');
            mApp.initPopover(element);
        } else if (element.hasClass('fc-time-grid-event')) {
            element.find('.fc-title').append('<div class="fc-description">' + event.description + '</div>');
        } else if (element.find('.fc-list-item-title').lenght !== 0) {
            element.find('.fc-list-item-title').append('<div class="fc-description">' + event.description + '</div>');
        }
    }
});
}
//== get search url
function GetSearchUrl() {
    return $('#search_url').attr('data-href');
}

//== set search url
function SetSearchUrl(search_params) {
    return $('#search_url').attr('href', GetSearchUrl() + search_params);
}

//== reload window
function ReloadSearchWindow() {
    window.location.href = $('#search_url').attr('href');
}

//== Class definition
function DateRange() {
    if ($('#m_dashboard_daterangepicker').length == 0) {
        return;
    }

    var picker = $('#m_dashboard_daterangepicker');
    // get search time
    var start = moment();
    var end = moment();

    function cb(start, end, label) {
        var title = '';
        var range = '';
        if ((end - start) < 100) {
            title = 'Today:';
            range = start.format('MMM D');
            SetSearchUrl('?start_date=' + start.format('YYYY-MM-DD') + '&end_date=')
        } else if (label == 'Yesterday') {
            title = 'Yesterday:';
            range = start.format('MMM D');
            SetSearchUrl('?start_date=' + start.format('YYYY-MM-DD') + '&end_date=')
        } else {
            range = start.format('MMM D') + ' - ' + end.format('MMM D');
            SetSearchUrl('?start_date=' + start.format('YYYY-MM-DD') + '&end_date=' + end.format('YYYY-MM-DD'))
        }

        picker.find('.m-subheader__daterange-date').html(range);
        picker.find('.m-subheader__daterange-title').html(title);
        //save search time
    }

    picker.daterangepicker({
        startDate: start,
        endDate: end,
        opens: 'left',
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end, '');
}