function CityHeatMap(heat_map) {
    if(heat_map.length == 0)
        return false;
    var areas = [];
    $.each(heat_map, function (key, value) {
        areas.push({
            id: value.heat_map_state,
            value: value.region_counter
        });
    });
    AmCharts.makeChart("city_heat_map", {
        "type": "map",
        "theme": "light",
        "colorSteps": 10,

        "dataProvider": {
            "map": "pakistanLow",
            "areas": areas
        },

        "areasSettings": {
            "autoZoom": true
        },

        "valueLegend": {
            "right": 10,
            "minValue": "little",
            "maxValue": "a lot!"
        },

        "export": {
            "enabled": true
        }

    });
}