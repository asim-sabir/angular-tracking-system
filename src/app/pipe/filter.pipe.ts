import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'filter'
})
export class FilterPipe implements PipeTransform {

    transform(value: any, args?: any, col?: string): any {
        if (args === undefined) return value;
        if (value === undefined) return value;
        if (args === 'all') return value;
        return value.filter(function (item) {
            for (let property in item) {
                if (col) {
                    property = col;
                }
                if (item[property] === null) {
                    continue;
                }
                if (item[property].toString().toLowerCase().includes(args.toLowerCase())) {
                    return true;
                }
            }
            return false;
        });
    }

}
