import {NgModule} from '@angular/core';
import {Level2Component} from './level-2.component';
import {Routes, RouterModule} from '@angular/router';
import {AuthGuard} from '../../_guards/auth.guard';
import {RouteGuard} from '../../_guards/route.guard';
import {PermissionsGuard} from '../../_guards/permissions.guard';

const routes: Routes = [
    {
        'path': 'level-2',
        'component': Level2Component,
        'canActivate': [
            AuthGuard,
            RouteGuard,
        ],
        'canActivateChild': [
            PermissionsGuard
        ],
        'children': [
            {
                path: 'hod',
                children: [
                    {
                        path: 'sale',
                        children: [
                            // index route
                            {
                                'path': 'index',
                                'loadChildren': './hod/sale2/home/dashboard.module#DashboardModule'
                            },
                            // profile routes
                            {
                                'path': 'profile',
                                'loadChildren': './hod/sale2/profile/view/view.module#ViewModule'
                            },
                            // client routes
                            {
                                'path': 'clients',
                                'loadChildren': './hod/sale2/client/index/index.module#IndexModule'
                            },
                            {
                                'path': 'client/new/individual',
                                'loadChildren': './hod/sale2/client/add/individual/add.module#AddModule'
                            },
                            {
                                'path': 'client/new/corporate',
                                'loadChildren': './hod/sale2/client/add/corporate/add.module#AddModule'
                            },
                            {
                                'path': 'client/update/individual/:client_id',
                                'loadChildren': './hod/sale2/client/update/individual/update.module#UpdateModule'
                            },
                            {
                                'path': 'client/update/corporate/:client_id',
                                'loadChildren': './hod/sale2/client/update/corporate/update.module#UpdateModule'
                            },
                            {
                                'path': 'client/view/:client_id',
                                'loadChildren': './hod/sale2/client/view/view.module#ViewModule'
                            },
                            // proposal routes
                            {
                                'path': 'proposals',
                                'loadChildren': './hod/sale2/proposal/index/index.module#IndexModule'
                            },
                            {
                                'path': 'proposal/new',
                                'loadChildren': './hod/sale2/proposal/add/add.module#AddModule'
                            },
                            // lead routes
                            {
                                'path': 'leads',
                                'loadChildren': './hod/sale2/lead/index/index.module#IndexModule'
                            },
                            {
                                'path': 'lead/new',
                                'loadChildren': './hod/sale2/lead/add/add.module#AddModule'
                            },
                            {
                                'path': 'lead/view/:lead_id',
                                'loadChildren': './hod/sale2/lead/view/view.module#ViewModule'
                            },
                            // sale routes
                            {
                                'path': 'sales',
                                'loadChildren': './hod/sale2/sale/index/index.module#IndexModule'
                            },
                            // report routes
                            {
                                'path': 'reports',
                                'loadChildren': './hod/sale2/report/index/index.module#IndexModule'
                            },
                            // sale agent routes
                            {
                                'path': 'agents',
                                'loadChildren': './hod/sale2/agent/index/index.module#IndexModule'
                            },
                            {
                                'path': 'agent/view/:agent_id',
                                'loadChildren': './hod/sale2/agent/view/view.module#ViewModule'
                            },
                            // ticket routes
                            {
                                'path': 'tickets',
                                'loadChildren': './hod/sale2/ticket/index/index.module#IndexModule'
                            },
                            {
                                'path': 'ticket/new',
                                'loadChildren': './hod/sale2/ticket/add/add.module#AddModule'
                            },
                            {
                                'path': 'ticket/view/:ticket_id',
                                'loadChildren': './hod/sale2/ticket/view/view.module#ViewModule'
                            },
                        ]
                    },
                    // crm routes
                    {
                        path: 'crm',
                        children: [
                            {
                                path: 'index',
                                loadChildren: './hod/crm2/home/dashboard.module#DashboardModule'
                            },
                            // profile routes
                            {
                                'path': 'profile',
                                'loadChildren': './hod/crm2/profile/view/view.module#ViewModule'
                            },
                            // client routes
                            {
                                'path': 'clients',
                                'loadChildren': './hod/crm2/client/index/index.module#IndexModule'
                            },
                            {
                                'path': 'client/update/individual/:opt_id/:client_id',
                                'loadChildren': './hod/crm2/client/update/individual/update.module#UpdateModule'
                            },
                            {
                                'path': 'client/update/corporate/:opt_id/:client_id',
                                'loadChildren': './hod/crm2/client/update/corporate/update.module#UpdateModule'
                            },
                            {
                                'path': 'client/update/emergency-contact/:opt_id/:client_id',
                                'loadChildren': './hod/crm2/client/update/e-contact/update.module#UpdateModule'
                            },
                            {
                                'path': 'client/update/vehicle/:opt_id/:vehicle_id',
                                'loadChildren': './hod/crm2/client/update/vehicle/update.module#UpdateModule'
                            },
                            {
                                'path': 'client/view/:client_id',
                                'loadChildren': './hod/crm2/client/view/view.module#ViewModule'
                            },
                            // lead routes
                            {
                                'path': 'leads',
                                'loadChildren': './hod/crm2/lead/index/index.module#IndexModule'
                            },
                            {
                                'path': 'lead/update/:lead_id',
                                'loadChildren': './hod/crm2/lead/update/update.module#UpdateModule'
                            },
                            // ticket routes
                            {
                                'path': 'tickets',
                                'loadChildren': './hod/crm2/ticket/index/index.module#IndexModule'
                            },
                            {
                                'path': 'ticket/new',
                                'loadChildren': './hod/crm2/ticket/add/add.module#AddModule'
                            },
                            {
                                'path': 'ticket/view/:ticket_id',
                                'loadChildren': './hod/crm2/ticket/view/view.module#ViewModule'
                            },
                            // operational ticket routes
                            {
                                'path': 'operational/tickets',
                                'loadChildren': './hod/crm2/operational-ticket/index/index.module#IndexModule'
                            },
                            {
                                'path': 'operational/ticket/new',
                                'loadChildren': './hod/crm2/operational-ticket/add/add.module#AddModule'
                            },
                            {
                                'path': 'operational/ticket/view/:ticket_id',
                                'loadChildren': './hod/crm2/operational-ticket/view/view.module#ViewModule'
                            },
                            // sale agent routes
                            {
                                'path': 'agents',
                                'loadChildren': './hod/crm2/agent/index/index.module#IndexModule'
                            },
                            {
                                'path': 'agent/view/:agent_id',
                                'loadChildren': './hod/crm2/agent/view/view.module#ViewModule'
                            },
                        ]
                    }
                ]
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class Level2RoutingModule {
}