import {Component, OnInit} from '@angular/core';
import {CommonFunctions} from '../../../../../../helpers/common.functions';
import {RequestUrls} from '../../../../../../helpers/request.urls';

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './index.component.html'
})

export class IndexComponent implements OnInit {
    _customer_list;
    status;
    type;
    filter;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls) {
    }

    ngOnInit() {
        this.LoadCustomerData();
    }

    LoadCustomerData() {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level2.hod.crm.client.index');
        this.c_f.HttpGet(url).subscribe(response => {
            if (response.isResponse) {
                this._customer_list = response.data;
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }
}
