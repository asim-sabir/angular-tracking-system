import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {CommonFunctions} from '../../../../../../../helpers/common.functions';
import {RequestUrls} from '../../../../../../../helpers/request.urls';

declare var $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './update.component.html'
})
export class UpdateComponent implements OnInit {
    _vehicle_detail;
    vehicle_id;
    opt_id;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private route: ActivatedRoute, private r_c: Router) {
        this._vehicle_detail = {};
    }

    ngOnInit() {
        // get route params
        this.route.params.subscribe(params => {
            this.vehicle_id = params['vehicle_id'];
            this.opt_id = params['opt_id'];
            this.LoadVehicleInfo(this.vehicle_id, this.opt_id);
        });
    }

    LoadVehicleInfo(vehicle_id, opt_id) {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level2.hod.crm.client.vehicle.get_update');
        this.c_f.HttpGet(url + '/' + opt_id + '/' + vehicle_id).subscribe(response => {
            if (response.isResponse) {
                this._vehicle_detail = response.data;
            } else {
                this.r_c.navigate(['/level-2/hod/crm/clients']);
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    SaveVehicle(event) {
        this.c_f.setLoading(true);
        this.c_f.HttpPut(this.r_u.getUrl('level2.hod.crm.client.vehicle.put'), $(event.target).serialize()).subscribe(response => {
            if (response.isResponse) {
                this.r_c.navigate(['/level-2/hod/crm/clients']);
                this.c_f.ShowToast(response.message, 'success');
            } else {
                this.c_f.setLoading(false);
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }
}
