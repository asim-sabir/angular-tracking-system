import {Component, OnInit} from '@angular/core';
import {CommonFunctions} from '../../../../../../helpers/common.functions';
import {RequestUrls} from '../../../../../../helpers/request.urls';
import {PriorityList} from '../../../../../../helpers/priority.list';

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './index.component.html'
})
export class IndexComponent implements OnInit {
    _ticket_list;
    _assigned_ticket_list;
    _priority_list;
    priority_val;
    status;
    filter;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls) {
    }

    ngOnInit() {
        this.LoadTicketData();
        this._priority_list = PriorityList.GetPlainPriorityList('ticket.priority');
    }

    LoadTicketData() {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level2.hod.crm.ticket.index');
        this.c_f.HttpGet(url).subscribe(response => {
            if (response.isResponse) {
                this._ticket_list = response.data.tickets;
                this._assigned_ticket_list = response.data.assigned_tickets;
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    TicketPriority(val) {
        return PriorityList.getPriority('ticket.priority.' + val + '.val');
    }

    TicketPriorityColor(val) {
        return PriorityList.getPriority('ticket.priority.' + val + '.color');
    }

    GenerateName(data) {
        let name = data.first_name;
        if (data.last_name) {
            name += ' ' + data.last_name;
        }
        return name;
    }

}
