import {Component, OnInit} from '@angular/core';
import {CommonFunctions} from '../../../../../../helpers/common.functions';
import {RequestUrls} from '../../../../../../helpers/request.urls';
import {StatusList} from '../../../../../../helpers/status.list';

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './index.component.html'
})
export class IndexComponent implements OnInit {
    _lead_list;
    _status_list;
    _agent_list;
    _assigned_agents;
    _new_agent;
    _lead_id;
    status;
    filter;
    private index;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls) {
        this._agent_list = [];
        this._assigned_agents = [];
    }

    ngOnInit() {
        this.LoadLeadData();
        this._status_list = StatusList.GetPlainStatusList('level3.lead');
    }

    LoadLeadData() {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level2.hod.crm.lead.index');
        this.c_f.HttpGet(url).subscribe(response => {
            if (response.isResponse) {
                this._lead_list = response.data;
                this.GetAssignedAgents();
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    LoadAgentsData(lead_id) {
        this._lead_id = lead_id;
        this.GetAssignedAgents();
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level2.hod.crm.lead.get_agent');
        this.c_f.HttpGet(url + '/' + lead_id).subscribe(response => {
            if (response.isResponse) {
                this._agent_list = response.data.agents;
                this.c_f.OpenModel('add-model-popup');
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    AssignAgent() {
        let data = {
            agent_id: this._new_agent,
            lead_id: this._lead_id
        };
        this.c_f.setLoading(true);
        this.c_f.HttpPut(this.r_u.getUrl('level2.hod.crm.lead.assign_agent'), data, 'json').subscribe(response => {
            if (response.isResponse) {
                this.LoadLeadData();
                this.c_f.ShowToast(response.message, 'success');
            } else {
                this.c_f.setLoading(false);
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.CloseModel('add-model-popup');
            this.c_f.setLoading(false);
        });
    }

    DeleteItem(item) {
        this.index = item;
    }

    ConfirmedDeleteItem() {
        let url = this.r_u.getUrl('level2.hod.crm.lead.remove_agent');
        this.c_f.HttpDelete(url + '/' + this.index.lead_agent_id).subscribe(response => {
            if (response.isResponse) {
                this.LoadLeadData();
                this.c_f.ShowToast(response.message, 'success');
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.CloseModel('delete-model-popup');
            this.c_f.setLoading(false);
        });
    }

    LeadStatus(val) {
        return StatusList.getStatus('level3.lead.' + val + '.val');
    }

    LeadStatusColor(val) {
        return StatusList.getStatus('level3.lead.' + val + '.color');
    }

    GenerateAssignedByName(data) {
        if (data) {
            let name = data.social_title + ' ' + data.first_name;
            if (data.last_name) {
                name += ' ' + data.last_name;
            }
            return name;
        }
    }

    GetAssignedAgents() {
        let self = this;
        this._lead_list.forEach(function (val, key) {
            if (val.lead_id == self._lead_id) {
                self._assigned_agents = val.agents;
            }
        });
    }
}
