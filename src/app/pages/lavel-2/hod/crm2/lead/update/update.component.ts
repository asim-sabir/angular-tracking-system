import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CommonFunctions} from '../../../../../../helpers/common.functions';
import {RequestUrls} from '../../../../../../helpers/request.urls';
import {ClientCurd} from '../../../../../../helpers/level-3/crm/lead/client-curd';
import {StatusList} from '../../../../../../helpers/status.list';

declare var $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './update.component.html',
})
export class UpdateComponent implements OnInit {
    _lead_detail;
    _lead_id;
    _cities_list;
    _countries_list;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private route: ActivatedRoute, private r_c: Router) {
        this._lead_detail = {};
    }

    ngOnInit() {
        // get route params
        this.route.params.subscribe(params => {
            this._lead_id = params['lead_id'];
            this.LoadLeadInfo(this._lead_id);
        });
        ClientCurd.init();
    }

    LoadLeadInfo(id) {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level2.hod.crm.lead.get_update');
        this.c_f.HttpGet(url + '/' + id).subscribe(response => {
            if (response.isResponse) {
                this._lead_detail = response.data.lead;
                this._countries_list = response.data.countries;
                this._cities_list = response.data.cities;
            } else {
                this.r_c.navigate(['/level-2/hod/crm/leads']);
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    SaveLead(event) {
        if (ClientCurd.ValidForm($(event.target))) {
            this.c_f.setLoading(true);
            this.c_f.HttpPut(this.r_u.getUrl('level2.hod.crm.lead.put'), $(event.target).serialize()).subscribe(response => {
                if (response.isResponse) {
                    this.r_c.navigate(['/level-2/hod/crm/leads']);
                    this.c_f.ShowToast(response.message, 'success');
                } else {
                    this.c_f.setLoading(false);
                    this.c_f.ShowToast(response.message, 'error');
                }
                this.c_f.setLoading(false);
            });
        }
    }

    LeadStatus(val) {
        return StatusList.getStatus('level3.lead.' + val + '.val');
    }

    CustomerType(data) {
        if (data) {
            if (data.client_info.customer_type == 0) {
                return 'Individual';
            } else if (data.client_info.customer_type == 1) {
                return 'Corporate';
            }
        }
        return '';
    }

    GenerateName(data) {
        let name = data.first_name;
        if (data.last_name) {
            name += ' ' + data.last_name;
        }
        return name;
    }

    EmergencyContactPerson(data, index) {
        if (data) {
            if (data.client_info.emergency_contact[index]) {
                return data.client_info.emergency_contact[index].contact_person_name;
            }
            return '';
        }
        return '';
    }

    EmergencyContactPersonRel(data, index) {
        if (data) {
            if (data.client_info.emergency_contact[index]) {
                return data.client_info.emergency_contact[index].contact_relation;
            }
            return '';
        }
        return '';
    }

    EmergencyContactPersonMobile(data, index) {
        if (data) {
            if (data.client_info.emergency_contact[index]) {
                return data.client_info.emergency_contact[index].mobile_no;
            }
            return '';
        }
        return '';
    }

    EmergencyContactPersonPhone(data, index) {
        if (data) {
            if (data.client_info.emergency_contact[index]) {
                return data.client_info.emergency_contact[index].phone_no;
            }
            return '';
        }
        return '';
    }

        NoOfVehicle(data) {
        if (data) {
            return data.length;
        }
        return 0;
    }
}
