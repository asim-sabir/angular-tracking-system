import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CommonFunctions} from '../../../../../../helpers/common.functions';
import {RequestUrls} from '../../../../../../helpers/request.urls';

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './view.component.html',
})
export class ViewComponent implements OnInit {
    _agent_detail;
    _clients;
    index;
    _client_id;
    private user_id;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private route: ActivatedRoute, private r_c: Router) {
        this._agent_detail = {};
        this._clients = {};
    }

    ngOnInit() {
        // get route params
        this.route.params.subscribe(params => {
            this.user_id = params['agent_id'];
            this.LoadUserInfo(this.user_id);
        });
    }

    LoadUserInfo(id) {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level2.hod.sale.agent.get');
        this.c_f.HttpGet(url + '/' + id).subscribe(response => {
            if (response.isResponse) {
                this._agent_detail = response.data.agents;
                this._clients = response.data.customers;
            } else {
                this.c_f.ShowToast(response.message, 'error');
                this.r_c.navigate(['/level-2/hod/sale/agents']);
            }
            this.c_f.setLoading(false);
        });
    }

    GenerateName(data) {
        let name = '';
        if (data.social_title) {
            name += data.social_title + ' ';
        }
        name += data.first_name
        if (data.last_name) {
            name += ' ' + data.last_name;
        }
        return name;
    }

    AssignClient() {
        let data = {
            user_id: this._agent_detail.user_id,
            customer_id: this._client_id
        };
        this.c_f.setLoading(true);
        this.c_f.HttpPut(this.r_u.getUrl('level2.hod.sale.agent.put'), data, 'json').subscribe(response => {
            if (response.isResponse) {
                this.LoadUserInfo(this.user_id);
                this.c_f.ShowToast(response.message, 'success');
            } else {
                this.c_f.setLoading(false);
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    DeleteItem(item) {
        this.index = item;
    }

    ConfirmedDeleteItem() {
        let url = this.r_u.getUrl('level2.hod.sale.agent.destroy');
        this.c_f.HttpDelete(url + '/' + this.index.id).subscribe(response => {
            if (response.isResponse) {
                let index = this._agent_detail.user_clients.indexOf(this.index);
                this._agent_detail.user_clients.splice(index, 1);
                this.LoadUserInfo(this.user_id);
                this.c_f.ShowToast(response.message, 'success');
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.CloseModel('delete-model-popup');
            this.c_f.setLoading(false);
        });
    }
}
