import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CommonFunctions} from '../../../../../../helpers/common.functions';
import {RequestUrls} from '../../../../../../helpers/request.urls';
import {TicketCurd} from '../../../../../../helpers/level-2/ticket/ticket-curd';
import {NotificationEmitter} from '../../../../../../helpers/emitters/notification.emitter';
import {PriorityList} from '../../../../../../helpers/priority.list';

declare let $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './add.component.html'
})
export class AddComponent implements OnInit {
    _client_list;
    _dept_list;
    _priority_list;

    constructor(private c_f: CommonFunctions,
                private r_u: RequestUrls,
                private n_e: NotificationEmitter,
                private router: Router) {
    }

    ngOnInit() {
        this.LoadData();
        this._priority_list = PriorityList.GetPlainPriorityList('ticket.priority');
    }

    LoadData() {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level2.hod.sale.ticket.get_add');
        this.c_f.HttpGet(url).subscribe(response => {
            if (response.isResponse) {
                this._client_list = response.data.client;
                this._dept_list = response.data.dept;
            } else {
                this.router.navigate(['level-2/hod/sale/tickets']);
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    SaveTicket(event) {
        if (TicketCurd.ValidForm($(event.target))) {
            this.c_f.setLoading(true);
            this.c_f.HttpPost(this.r_u.getUrl('level2.hod.sale.ticket.post'), $(event.target).serialize()).subscribe(response => {
                if (response.isResponse) {
                    this.n_e.change(true);
                    this.router.navigate(['level-2/hod/sale/tickets']);
                    this.c_f.ShowToast(response.message, 'success');
                } else {
                    this.c_f.setLoading(false);
                    this.c_f.ShowToast(response.message, 'error');
                }
                this.c_f.setLoading(false);
            });
        }
    }
}
