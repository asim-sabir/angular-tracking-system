import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CommonFunctions} from '../../../../../../helpers/common.functions';
import {RequestUrls} from '../../../../../../helpers/request.urls';
import {LeadFollowUpCurd} from '../../../../../../helpers/level-2/lead-follow-up/lead-follow-up-curd';
import {StatusList} from '../../../../../../helpers/status.list';

declare var $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './view.component.html',
})
export class ViewComponent implements OnInit {
    _lead_detail;
    _agent_list;
    _lead_id;
    _status_list;
    _new_agent;
    index;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private route: ActivatedRoute, private r_c: Router) {
        this._lead_detail = {};
    }

    ngOnInit() {
        // get route params
        this.route.params.subscribe(params => {
            this._lead_id = params['lead_id'];
            this.LoadLeadInfo(this._lead_id);
        });
        this._status_list = StatusList.GetPlainStatusList('level3.lead', 1);
    }

    LoadLeadInfo(id) {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level2.hod.sale.lead.info');
        this.c_f.HttpGet(url + '/' + id).subscribe(response => {
            if (response.isResponse) {
                this._lead_detail = response.data.lead;
                this._agent_list = response.data.agents;
            } else {
                this.r_c.navigate(['level-2/hod/sale/leads']);
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    LeadStatus(val) {
        return StatusList.getStatus('level3.lead.' + val + '.val');
    }

    SaveComment(event) {
        if (LeadFollowUpCurd.ValidForm($(event.target))) {
            this.c_f.setLoading(true);
            this.c_f.HttpPost(this.r_u.getUrl('level2.hod.sale.lead.followup.post'), $(event.target).serialize()).subscribe(response => {
                if (response.isResponse) {
                    this.LoadLeadInfo(this._lead_id);
                    this.c_f.ShowToast(response.message, 'success');
                } else {
                    this.c_f.setLoading(false);
                    this.c_f.ShowToast(response.message, 'error');
                }
                this.c_f.setLoading(false);
            });
        }
    }

    UpdateStatus() {
        let data = {
            lead_id: this._lead_id,
            status: this._lead_detail.status
        };
        this.c_f.setLoading(true);
        this.c_f.HttpPut(this.r_u.getUrl('level2.hod.sale.lead.status_put'), data, 'json').subscribe(response => {
            if (response.isResponse) {
                this.LoadLeadInfo(this._lead_id);
                this.c_f.ShowToast(response.message, 'success');
            } else {
                this.c_f.setLoading(false);
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    AssignAgent() {
        let data = {
            agent_id: this._new_agent,
            lead_id: this._lead_detail.lead_id
        };
        this.c_f.setLoading(true);
        this.c_f.HttpPut(this.r_u.getUrl('level2.hod.sale.lead.assign_agent'), data, 'json').subscribe(response => {
            if (response.isResponse) {
                this.LoadLeadInfo(this._lead_id);
                this.c_f.ShowToast(response.message, 'success');
            } else {
                this.c_f.setLoading(false);
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.CloseModel('add-model-popup');
            this.c_f.setLoading(false);
        });
    }

    DeleteItem(item) {
        this.index = item;
    }

    ConfirmedDeleteItem() {
        let url = this.r_u.getUrl('level2.hod.sale.lead.remove_agent');
        this.c_f.HttpDelete(url + '/' + this.index.lead_agent_id).subscribe(response => {
            if (response.isResponse) {
                let index = this._lead_detail.agents.indexOf(this.index);
                this._lead_detail.agents.splice(index, 1);
                this.LoadLeadInfo(this._lead_id);
                this.c_f.ShowToast(response.message, 'success');
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.CloseModel('delete-model-popup');
            this.c_f.setLoading(false);
        });
    }

    GenerateName(data) {
        let name = data.first_name;
        if (data.last_name) {
            name += ' ' + data.last_name;
        }
        return name;
    }

    GenerateLink(data) {
        if (data) {
            if (data.pdf_file_path) {
                return data.pdf_file_path;
            }
        }
        return 'level-2/hod/sale/proposals';
    }
}
