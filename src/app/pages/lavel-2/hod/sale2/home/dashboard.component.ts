import {Component, OnInit} from '@angular/core';
import {CommonFunctions} from '../../../../../helpers/common.functions';
import {RequestUrls} from '../../../../../helpers/request.urls';
import {StatusList} from '../../../../../helpers/status.list';

declare var dailySales: any;
declare var ProfitChart: any;
declare var EventInit: any;
declare var DateRange: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {
    _data;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls) {
        this._data = {};
    }

    ngOnInit() {
        this.LoadData();
        DateRange();
    }

    LoadData() {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level2.hod.sale.dashboard.get');
        this.c_f.HttpGet(url).subscribe(response => {
            if (response.isResponse) {
                dailySales(response.data.monthly_sale);
                ProfitChart(response.data.total_clients, response.data.total_sales, response.data.total_leads);
                this._data = response.data;
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    LeadStatus(val) {
        return StatusList.getStatus('level3.lead.' + val + '.val');
    }

    LeadStatusColor(val) {
        return StatusList.getStatus('level3.lead.' + val + '.color');
    }
}
