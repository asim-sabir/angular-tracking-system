import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CommonFunctions} from '../../../../../../helpers/common.functions';
import {RequestUrls} from '../../../../../../helpers/request.urls';
import {UserCurd} from '../../../../../../helpers/super-admin/user/user-curd';
import {DataSharing} from '../../../../../../helpers/data.sharing';
import {Emitters} from '../../../../../../helpers/emitters/emitters';

declare var $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './view.component.html',
})
export class ViewComponent implements OnInit {
    _user_detail;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private r_c: Router, private e_c: Emitters) {
        this._user_detail = {};
    }

    ngOnInit() {
        this.LoadUserInfo();
    }

    LoadUserInfo() {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level2.hod.sale.user.info');
        this.c_f.HttpGet(url).subscribe(response => {
            if (response.isResponse) {
                this._user_detail = response.data;
            } else {
                this.r_c.navigate(['level-2/hod/sale/clients']);
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    SaveUser(event) {
        if (UserCurd.ValidForm($(event.target))) {
            this.c_f.setLoading(true);
            this.c_f.HttpPut(this.r_u.getUrl('level2.hod.sale.user.put'), $(event.target).serialize()).subscribe(response => {
                if (response.isResponse) {
                    DataSharing.store('info', JSON.stringify(response.data));
                    this.c_f.ShowToast(response.message, 'success');
                    this.e_c.change(response.data);
                } else {
                    this.c_f.setLoading(false);
                    this.c_f.ShowToast(response.message, 'error');
                }
                this.c_f.setLoading(false);
            });
        }
    }
}
