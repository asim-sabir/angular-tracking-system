import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {DefaultComponent} from '../../../../../../layouts/level-2/default/default.component';
import {Level2LayoutModule} from '../../../../../../layouts/level-2/level-2-layout.module';
import {AddComponent} from './add.component';

const routes: Routes = [
    {
        path: '',
        component: DefaultComponent,
        children: [
            {
                path: '',
                component: AddComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), Level2LayoutModule
    ], exports: [
        RouterModule
    ],
    declarations: [AddComponent]
})
export class AddModule {
}
