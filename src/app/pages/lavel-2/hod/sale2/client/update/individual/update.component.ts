import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {CommonFunctions} from '../../../../../../../helpers/common.functions';
import {RequestUrls} from '../../../../../../../helpers/request.urls';
import {ClientCurd} from '../../../../../../../helpers/level-2/client/client-curd';

declare var $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './update.component.html'
})
export class UpdateComponent implements OnInit {
    _client_detail;
    _cities_list;
    _countries_list;
    private client_id;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private route: ActivatedRoute, private r_c: Router) {
        this._client_detail = {};
    }

    ngOnInit() {
        // get route params
        this.route.params.subscribe(params => {
            this.client_id = params['client_id'];
            this.LoadClientInfo(this.client_id);
        });
    }

    LoadClientInfo(id) {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level2.hod.sale.client.get_update');
        this.c_f.HttpGet(url + '/' + id).subscribe(response => {
            if (response.isResponse) {
                this._countries_list = response.data.countries;
                this._cities_list = response.data.cities;
                this._client_detail = response.data.client;
            } else {
                this.r_c.navigate(['level-2/hod/sale/clients']);
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    SaveClient(event) {
        if (ClientCurd.ValidForm($(event.target))) {
            this.c_f.setLoading(true);
            this.c_f.HttpPut(this.r_u.getUrl('level2.hod.sale.client.put'), $(event.target).serialize()).subscribe(response => {
                if (response.isResponse) {
                    this.r_c.navigate(['level-2/hod/sale/clients']);
                    this.c_f.ShowToast(response.message, 'success');
                } else {
                    this.c_f.setLoading(false);
                    this.c_f.ShowToast(response.message, 'error');
                }
                this.c_f.setLoading(false);
            });
        }
    }

    SwitchForm() {
        this.r_c.navigate(['level-2/hod/sale/client/update/corporate/' + this.client_id]);
    }
}
