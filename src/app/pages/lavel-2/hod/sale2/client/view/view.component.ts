import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CommonFunctions} from '../../../../../../helpers/common.functions';
import {RequestUrls} from '../../../../../../helpers/request.urls';
import {ClientFollowUpCurd} from '../../../../../../helpers/level-2/client-follow-up/client-follow-up-curd';

declare var $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './view.component.html',
})
export class ViewComponent implements OnInit {
    _agent_list;
    _customer_detail;
    _customer_id;
    _new_agent;
    private index;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private route: ActivatedRoute, private r_c: Router) {
        this._customer_detail = {};
    }

    ngOnInit() {
        // get route params
        this.route.params.subscribe(params => {
            this._customer_id = params['client_id'];
            this.LoadClientInfo(this._customer_id);
        });
    }

    LoadClientInfo(id) {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level2.hod.sale.client.info');
        this.c_f.HttpGet(url + '/' + id).subscribe(response => {
            if (response.isResponse) {
                this._customer_detail = response.data.client_info;
                this._agent_list = response.data.agents;
            } else {
                this.r_c.navigate(['level-2/hod/sale/clients']);
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    SaveComment(event) {
        if (ClientFollowUpCurd.ValidForm($(event.target))) {
            this.c_f.setLoading(true);
            this.c_f.HttpPost(this.r_u.getUrl('level2.hod.sale.client.followup.post'), $(event.target).serialize()).subscribe(response => {
                if (response.isResponse) {
                    this.LoadClientInfo(this._customer_id);
                    this.c_f.ShowToast(response.message, 'success');
                } else {
                    this.c_f.setLoading(false);
                    this.c_f.ShowToast(response.message, 'error');
                }
                this.c_f.setLoading(false);
            });
        }
    }

    AssignAgent() {
        let data = {
            agent_id: this._new_agent,
            customer_id: this._customer_detail.customer_id
        };
        this.c_f.setLoading(true);
        this.c_f.HttpPut(this.r_u.getUrl('level2.hod.sale.client.assign_agent'), data, 'json').subscribe(response => {
            if (response.isResponse) {
                this.LoadClientInfo(this._customer_id);
                this.c_f.ShowToast(response.message, 'success');
            } else {
                this.c_f.setLoading(false);
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.CloseModel('add-model-popup');
            this.c_f.setLoading(false);
        });
    }

    DeleteItem(item) {
        this.index = item;
    }

    ConfirmedDeleteItem() {
        let url = this.r_u.getUrl('level2.hod.sale.client.remove_agent');
        this.c_f.HttpDelete(url + '/' + this.index.user_id + '/' + this._customer_detail.customer_id).subscribe(response => {
            if (response.isResponse) {
                let index = this._customer_detail.client_agents.indexOf(this.index);
                this._customer_detail.client_agents.splice(index, 1);
                this.LoadClientInfo(this._customer_id);
                this.c_f.ShowToast(response.message, 'success');
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.CloseModel('delete-model-popup');
            this.c_f.setLoading(false);
        });
    }

    LoadEditUrl(type, id) {
        if (type == 0) {
            return '/level-2/hod/sale/client/update/individual/' + id;
        }
        return '/level-2/hod/sale/client/update/corporate/' + id;
    }

    GenerateCustomerName(data) {
        let name = data.customer_name;
        if (data.social_title) {
            name = data.social_title + ' ' + name;
        }
        return name;
    }

    GenerateName(data) {
        let name = data.first_name;
        if (data.last_name) {
            name += ' ' + data.last_name;
        }
        return name;
    }
}
