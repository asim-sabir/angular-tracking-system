import {Component, OnInit} from '@angular/core';
import {CommonFunctions} from '../../../../../../helpers/common.functions';
import {RequestUrls} from '../../../../../../helpers/request.urls';

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './index.component.html'
})
export class IndexComponent implements OnInit {
    _customer_list;
    status;
    type;
    filter;
    private copy_customer_list;
    private index;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls) {
    }

    ngOnInit() {
        this.LoadCustomerData();
        // this.s_f.sendMessage('Allah', 'Allah');
    }

    LoadCustomerData() {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level2.hod.sale.client.index');
        this.c_f.HttpGet(url).subscribe(response => {
            if (response.isResponse) {
                this._customer_list = response.data;
                this.copy_customer_list = this._customer_list;
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    LoadEditUrl(type, id) {
        if (type == 0) {
            return '/level-2/hod/sale/client/update/individual/' + id;
        }
        return '/level-2/hod/sale/client/update/corporate/' + id;
    }

    DeleteItem(item) {
        this.index = item;
    }

    ConfirmedDeleteItem() {
        let url = this.r_u.getUrl('level2.hod.sale.client.destroy');
        this.c_f.HttpDelete(url + '/' + this.index.customer_id).subscribe(response => {
            if (response.isResponse) {
                let index = this._customer_list.indexOf(this.index);
                this._customer_list.splice(index, 1);
                this.c_f.ShowToast(response.message, 'success');
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.CloseModel('delete-model-popup');
            this.c_f.setLoading(false);
        });
    }
}
