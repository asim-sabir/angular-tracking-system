import {Component, OnInit} from '@angular/core';
import {LoginCustom} from '../../helpers/login-custom';
import {LoginService} from '../../services/login.service';
import {Router, ActivatedRoute} from '@angular/router';
import {CommonFunctions} from '../../helpers/common.functions';
import {DataSharing} from '../../helpers/data.sharing';

declare var $: any;

@Component({
    selector: '.m-grid.m-grid--hor.m-grid--root.m-page',
    templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
    returnUrl: string;

    constructor(private login_service: LoginService,
                private router: ActivatedRoute, private common_function: CommonFunctions, private r_c: Router) {

    }

    ngOnInit() {
        LoginCustom.init();
        this.common_function.setLoading(true);
        // this.returnUrl = this.router.snapshot.queryParams['returnUrl'];
        this.login_service.CheckLogin(function (response) {
            if (response) {
                if (response.isResponse) {
                    DataSharing.StoreJsonObj('info', response.data);
                    this.returnUrl = DataSharing.GetLandingUrl(response.data)
                    this.r_c.navigate([this.returnUrl]);
                } else {
                    this.common_function.ShowMessage(response.message, 'danger');
                }
            }
            this.common_function.setLoading(false);
        }.bind(this));
    }

    login(event) {
        this.login_service.UserLogin($(event.target).serialize(), this.returnUrl);
    }
}
