import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {DefaultComponent} from '../../../../layouts/level-3/default/default.component';
import {Level3LayoutModule} from '../../../../layouts/level-3/level-3-layout.module';
import {SaleHomeComponent} from './sale.home.component';

const routes: Routes = [
    {
        'path': '',
        'component': DefaultComponent,
        children: [
            {
                path: '',
                component: SaleHomeComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), Level3LayoutModule
    ], exports: [RouterModule],
    declarations: [SaleHomeComponent]
})
export class SaleHomeModule {
}
