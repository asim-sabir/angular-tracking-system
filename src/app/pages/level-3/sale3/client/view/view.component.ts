import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CommonFunctions} from '../../../../../helpers/common.functions';
import {RequestUrls} from '../../../../../helpers/request.urls';
import {ClientFollowUpCurd} from '../../../../../helpers/level-3/client-follow-up/client-follow-up-curd';

declare var $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './view.component.html',
})
export class ViewComponent implements OnInit {
    _customer_detail;
    _customer_id;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private route: ActivatedRoute, private r_c: Router) {
        this._customer_detail = {};
    }

    ngOnInit() {
        // get route params
        this.route.params.subscribe(params => {
            this._customer_id = params['client_id'];
            this.LoadClientInfo(this._customer_id);
        });
    }

    LoadClientInfo(id) {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level3.sale.client.info');
        this.c_f.HttpGet(url + '/' + id).subscribe(response => {
            if (response.isResponse) {
                this._customer_detail = response.data;
            } else {
                this.r_c.navigate(['level-3/sale/clients']);
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    SaveComment(event) {
        if (ClientFollowUpCurd.ValidForm($(event.target))) {
            this.c_f.setLoading(true);
            this.c_f.HttpPost(this.r_u.getUrl('level3.sale.client.followup.post'), $(event.target).serialize()).subscribe(response => {
                if (response.isResponse) {
                    this.LoadClientInfo(this._customer_id);
                    this.c_f.ShowToast(response.message, 'success');
                } else {
                    this.c_f.setLoading(false);
                    this.c_f.ShowToast(response.message, 'error');
                }
                this.c_f.setLoading(false);
            });
        }
    }

    LoadEditUrl(type, id) {
        if (type == 0) {
            return '/level-3/sale/client/update/individual/' + id;
        }
        return '/level-3/sale/client/update/corporate/' + id;
    }

    GenerateCutomerName(data) {
        let name = data.customer_name;
        if (data.social_title) {
            name = data.social_title + ' ' + name;
        }
        return name;
    }

    GenerateName(data) {
        let name = data.first_name;
        if (data.last_name) {
            name += ' ' + data.last_name;
        }
        return name;
    }
}
