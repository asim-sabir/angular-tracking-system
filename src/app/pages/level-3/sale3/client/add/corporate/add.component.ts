import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CommonFunctions} from '../../../../../../helpers/common.functions';
import {RequestUrls} from '../../../../../../helpers/request.urls';
import {ClientCurd} from '../../../../../../helpers/level-3/client/client-curd';
import {NotificationEmitter} from '../../../../../../helpers/emitters/notification.emitter';

declare let $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './add.component.html'
})
export class AddComponent implements OnInit {
    _cities_list;
    _countries_list;

    constructor(private c_f: CommonFunctions,
                private r_u: RequestUrls,
                private n_e: NotificationEmitter,
                private router: Router) {
    }

    ngOnInit() {
        this.LoadData();
    }

    LoadData() {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level3.sale.client.get_add');
        this.c_f.HttpGet(url).subscribe(response => {
            if (response.isResponse) {
                this._countries_list = response.data.countries;
                this._cities_list = response.data.cities;
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    SaveClient(event) {
        if (ClientCurd.ValidForm($(event.target))) {
            this.c_f.setLoading(true);
            this.c_f.HttpPost(this.r_u.getUrl('level3.sale.client.post'), $(event.target).serialize()).subscribe(response => {
                if (response.isResponse) {
                    this.n_e.change(true);
                    this.router.navigate(['level-3/sale/clients']);
                    this.c_f.ShowToast(response.message, 'success');
                } else {
                    this.c_f.setLoading(false);
                    this.c_f.ShowToast(response.message, 'error');
                }
                this.c_f.setLoading(false);
            });
        }
    }

    SwitchForm() {
        this.router.navigate(['level-3/sale/client/new/individual']);
    }
}
