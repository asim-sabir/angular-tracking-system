import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CommonFunctions} from '../../../../../helpers/common.functions';
import {RequestUrls} from '../../../../../helpers/request.urls';

declare var $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './index.component.html'
})
export class IndexComponent implements OnInit {

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private r_c: Router) {
    }

    ngOnInit() {
    }

    GenerateReport(event) {
        let url = this.r_u.getUrl('level3.sale.report.get');
        this.c_f.setLoading(true);
        this.c_f.HttpPost(url, $(event.target).serialize()).subscribe(response => {
            if (response.isResponse) {
                console.log(response);
                window.location.href = response.data;
                this.c_f.ShowToast(response.message, 'success');
            } else {
                this.c_f.setLoading(false);
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }
}
