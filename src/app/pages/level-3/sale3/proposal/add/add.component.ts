import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CommonFunctions} from '../../../../../helpers/common.functions';
import {RequestUrls} from '../../../../../helpers/request.urls';
import {ProposalCurd} from '../../../../../helpers/level-3/proposal/proposal-curd';
import {NotificationEmitter} from '../../../../../helpers/emitters/notification.emitter';

declare let $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './add.component.html'
})
export class AddComponent implements OnInit {
    _customers_list;
    _packages_list;

    constructor(private c_f: CommonFunctions,
                private r_u: RequestUrls,
                private n_e: NotificationEmitter,
                private router: Router) {
    }

    ngOnInit() {
        this.LoadData();
    }

    LoadData() {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level3.sale.proposal.get_add');
        this.c_f.HttpGet(url).subscribe(response => {
            if (response.isResponse) {
                this._customers_list = response.data.clients;
                this._packages_list = response.data.packages;
            } else {
                this.router.navigate(['level-3/sale/proposals']);
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    SaveProposal(event) {
        if (ProposalCurd.ValidForm($(event.target))) {
            this.c_f.setLoading(true);
            this.c_f.HttpPost(this.r_u.getUrl('level3.sale.proposal.post'), $(event.target).serialize()).subscribe(response => {
                if (response.isResponse) {
                    this.router.navigate(['level-3/sale/proposals']);
                    window.open(response.data, '_blank');
                    this.n_e.change(true);
                    this.c_f.ShowToast(response.message, 'success');
                } else {
                    this.c_f.setLoading(false);
                    this.c_f.ShowToast(response.message, 'error');
                }
                this.c_f.setLoading(false);
            });
        }
    }
}
