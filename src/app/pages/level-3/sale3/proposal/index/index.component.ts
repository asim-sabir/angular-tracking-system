import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CommonFunctions} from '../../../../../helpers/common.functions';
import {RequestUrls} from '../../../../../helpers/request.urls';
import {NotificationEmitter} from '../../../../../helpers/emitters/notification.emitter';

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './index.component.html'
})
export class IndexComponent implements OnInit {
    _proposal_list;
    status;
    type;
    filter;
    private index;

    constructor(private c_f: CommonFunctions,
                private r_u: RequestUrls,
                private n_e: NotificationEmitter,
                private r_c: Router) {
    }

    ngOnInit() {
        this.LoadProposalData();
    }

    LoadProposalData() {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level3.sale.proposal.index');
        this.c_f.HttpGet(url).subscribe(response => {
            if (response.isResponse) {
                this._proposal_list = response.data;
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    GenerateLead(item) {
        let url = this.r_u.getUrl('level3.sale.lead.post');
        this.c_f.HttpPost(url, {proposal_id: item.proposal_id}, 'json').subscribe(response => {
            if (response.isResponse) {
                this.n_e.change(true);
                this.r_c.navigate(['level-3/sale/leads']);
                this.c_f.ShowToast(response.message, 'success');
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    DeleteItem(item) {
        this.index = item;
    }

    ConfirmedDeleteItem() {
        let url = this.r_u.getUrl('level3.sale.proposal.destroy');
        this.c_f.HttpDelete(url + '/' + this.index.proposal_id).subscribe(response => {
            if (response.isResponse) {
                let index = this._proposal_list.indexOf(this.index);
                this._proposal_list.splice(index, 1);
                this.c_f.ShowToast(response.message, 'success');
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.CloseModel('delete-model-popup');
            this.c_f.setLoading(false);
        });
    }
}
