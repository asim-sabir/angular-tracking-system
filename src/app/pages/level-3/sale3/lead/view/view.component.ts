import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CommonFunctions} from '../../../../../helpers/common.functions';
import {RequestUrls} from '../../../../../helpers/request.urls';
import {LeadFollowUpCurd} from '../../../../../helpers/level-3/lead-follow-up/lead-follow-up-curd';
import {StatusList} from '../../../../../helpers/status.list';

declare var $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './view.component.html',
})
export class ViewComponent implements OnInit {
    _lead_detail;
    _lead_id;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private route: ActivatedRoute, private r_c: Router) {
        this._lead_detail = {};
    }

    ngOnInit() {
        // get route params
        this.route.params.subscribe(params => {
            this._lead_id = params['lead_id'];
            this.LoadLeadInfo(this._lead_id);
        });
    }

    LoadLeadInfo(id) {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level3.sale.lead.info');
        this.c_f.HttpGet(url + '/' + id).subscribe(response => {
            if (response.isResponse) {
                this._lead_detail = response.data;
            } else {
                this.r_c.navigate(['level-3/sale/leads']);
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    LeadStatus(val) {
        return StatusList.getStatus('level3.lead.' + val + '.val');
    }

    SaveComment(event) {
        if (LeadFollowUpCurd.ValidForm($(event.target))) {
            this.c_f.setLoading(true);
            this.c_f.HttpPost(this.r_u.getUrl('level3.sale.lead.followup.post'), $(event.target).serialize()).subscribe(response => {
                if (response.isResponse) {
                    this.LoadLeadInfo(this._lead_id);
                    this.c_f.ShowToast(response.message, 'success');
                } else {
                    this.c_f.setLoading(false);
                    this.c_f.ShowToast(response.message, 'error');
                }
                this.c_f.setLoading(false);
            });
        }
    }

    GenerateName(data) {
        let name = data.first_name;
        if (data.last_name) {
            name += ' ' + data.last_name;
        }
        return name;
    }

    GenerateLink(data) {
        if (data) {
            if (data.pdf_file_path) {
                return data.pdf_file_path;
            }
        }
        return 'level-3/sale/proposals';
    }
}
