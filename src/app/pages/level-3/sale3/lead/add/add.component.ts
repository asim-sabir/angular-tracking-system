import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CommonFunctions} from '../../../../../helpers/common.functions';
import {RequestUrls} from '../../../../../helpers/request.urls';
import {LeadCurd} from '../../../../../helpers/level-3/lead/lead-curd';
import {NotificationEmitter} from '../../../../../helpers/emitters/notification.emitter';

declare let $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './add.component.html'
})
export class AddComponent implements OnInit {
    _proposal_list;

    constructor(private c_f: CommonFunctions,
                private r_u: RequestUrls,
                private n_e: NotificationEmitter,
                private router: Router) {
    }

    ngOnInit() {
        this.LoadData();
    }

    LoadData() {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level3.sale.lead.get_add');
        this.c_f.HttpGet(url).subscribe(response => {
            if (response.isResponse) {
                this._proposal_list = response.data;
            } else {
                this.router.navigate(['/level-3/sale/leads']);
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    SaveLead(event) {
        if (LeadCurd.ValidForm($(event.target))) {
            this.c_f.setLoading(true);
            this.c_f.HttpPost(this.r_u.getUrl('level3.sale.lead.post'), $(event.target).serialize()).subscribe(response => {
                if (response.isResponse) {
                    this.n_e.change(true);
                    this.router.navigate(['/level-3/sale/leads']);
                    this.c_f.ShowToast(response.message, 'success');
                } else {
                    this.c_f.setLoading(false);
                    this.c_f.ShowToast(response.message, 'error');
                }
                this.c_f.setLoading(false);
            });
        }
    }
}
