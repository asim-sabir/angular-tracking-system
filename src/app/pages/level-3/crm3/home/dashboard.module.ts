import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {DefaultComponent} from '../../../../layouts/level-3/default/default.component';
import {Level3LayoutModule} from '../../../../layouts/level-3/level-3-layout.module';
import {DashboardComponent} from './dashboard.component';

const routes: Routes = [
    {
        'path': '',
        'component': DefaultComponent,
        children: [
            {
                path: '',
                component: DashboardComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), Level3LayoutModule
    ], exports: [RouterModule],
    declarations: [DashboardComponent]
})
export class DashboardModule {
}
