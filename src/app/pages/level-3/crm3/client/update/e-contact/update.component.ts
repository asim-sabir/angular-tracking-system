import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {CommonFunctions} from '../../../../../../helpers/common.functions';
import {RequestUrls} from '../../../../../../helpers/request.urls';
import {ClientCurd} from '../../../../../../helpers/level-3/client/client-curd';

declare var $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './update.component.html'
})
export class UpdateComponent implements OnInit {
    _contact_detail;
    client_id;
    opt_id;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private route: ActivatedRoute, private r_c: Router) {
        this._contact_detail = {};
    }

    ngOnInit() {
        // get route params
        this.route.params.subscribe(params => {
            this.client_id = params['client_id'];
            this.opt_id = params['opt_id'];
            this.LoadClientInfo(this.client_id, this.opt_id);
        });
    }

    LoadClientInfo(client_id, opt_id) {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level3.crm.client.emergency_contact.get_update');
        this.c_f.HttpGet(url + '/' + opt_id + '/' + client_id).subscribe(response => {
            if (response.isResponse) {
                this._contact_detail = response.data;
            } else {
                this.r_c.navigate(['level-3/crm/clients']);
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    SaveClient(event) {
        if (ClientCurd.ValidForm($(event.target))) {
            this.c_f.setLoading(true);
            this.c_f.HttpPut(this.r_u.getUrl('level3.crm.client.emergency_contact.put'), $(event.target).serialize()).subscribe(response => {
                if (response.isResponse) {
                    this.r_c.navigate(['level-3/crm/clients']);
                    this.c_f.ShowToast(response.message, 'success');
                } else {
                    this.c_f.setLoading(false);
                    this.c_f.ShowToast(response.message, 'error');
                }
                this.c_f.setLoading(false);
            });
        }
    }

    EmergencyContactPerson(data, index) {
        if (data) {
            if (data[index]) {
                return data[index].contact_person_name;
            }
            return '';
        }
        return '';
    }

    EmergencyContactPersonRel(data, index) {
        if (data) {
            if (data[index]) {
                return data[index].contact_relation;
            }
            return '';
        }
        return '';
    }

    EmergencyContactPersonMobile(data, index) {
        if (data) {
            if (data[index]) {
                return data[index].mobile_no;
            }
            return '';
        }
        return '';
    }

    EmergencyContactPersonPhone(data, index) {
        if (data) {
            if (data[index]) {
                return data[index].phone_no;
            }
            return '';
        }
        return '';
    }
}
