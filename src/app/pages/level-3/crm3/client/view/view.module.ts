import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {DefaultComponent} from '../../../../../layouts/level-3/default/default.component';
import {ViewComponent} from './view.component';
import {Level3LayoutModule} from '../../../../../layouts/level-3/level-3-layout.module';

const routes: Routes = [
    {
        path: '',
        component: DefaultComponent,
        children: [
            {
                path: '',
                component: ViewComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), Level3LayoutModule
    ], exports: [
        RouterModule
    ],
    declarations: [ViewComponent]
})
export class ViewModule { }
