import {Component, OnInit} from '@angular/core';
import {CommonFunctions} from '../../../../../helpers/common.functions';
import {RequestUrls} from '../../../../../helpers/request.urls';
import {StatusList} from '../../../../../helpers/status.list';

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './index.component.html'
})
export class IndexComponent implements OnInit {
    _lead_list;
    _status_list;
    status;
    filter;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls) {
    }

    ngOnInit() {
        this.LoadLeadData();
        this._status_list = StatusList.GetPlainStatusList('level3.lead');
    }

    LoadLeadData() {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level3.crm.lead.index');
        this.c_f.HttpGet(url).subscribe(response => {
            if (response.isResponse) {
                this._lead_list = response.data;
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    LeadStatus(val) {
        return StatusList.getStatus('level3.lead.' + val + '.val');
    }

    LeadStatusColor(val) {
        return StatusList.getStatus('level3.lead.' + val + '.color');
    }

    GenerateAssignedByName(data) {
        if (data) {
            let name = data.social_title + ' ' + data.first_name;
            if (data.last_name) {
                name += ' ' + data.last_name;
            }
            return name;
        }
    }
}
