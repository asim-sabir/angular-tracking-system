import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CommonFunctions} from '../../../../../helpers/common.functions';
import {RequestUrls} from '../../../../../helpers/request.urls';
import {PriorityList} from '../../../../../helpers/priority.list';
import {TicketStatusList} from '../../../../../helpers/ticket.status.list';

declare var $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './view.component.html',
})
export class ViewComponent implements OnInit {
    _ticket_detail;
    _user_list;
    _ticket_id;
    commented_by;
    comment;
    _priority_list;
    private index;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private route: ActivatedRoute, private r_c: Router) {
        this._ticket_detail = {};
    }

    ngOnInit() {
        // get route params
        this.route.params.subscribe(params => {
            this._ticket_id = params['ticket_id'];
            this.LoadTicketInfo(this._ticket_id);
        });
        this._priority_list = PriorityList.GetPlainPriorityList('ticket.priority');
    }

    LoadTicketInfo(id) {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level3.crm.ticket.operational.info');
        this.c_f.HttpGet(url + '/' + id).subscribe(response => {
            if (response.isResponse) {
                this._ticket_detail = response.data.ticket;
            } else {
                // this.r_c.navigate(['level-2/manager/sale/tickets']);
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    SaveComment() {
        let data = {
            ticket_id: this._ticket_detail.ticket_id,
            customer_id: this._ticket_detail.customer_info.customer_id,
            commented_by: this.commented_by,
            comment: this.comment,
        };
        this.c_f.setLoading(true);
        this.c_f.HttpPost(this.r_u.getUrl('level3.crm.ticket.operational.followup.post'), data, 'json').subscribe(response => {
            if (response.isResponse) {
                this.LoadTicketInfo(this._ticket_id);
                this.c_f.ShowToast(response.message, 'success');
            } else {
                this.c_f.setLoading(false);
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    UpdatePriority(event) {
        this.c_f.setLoading(true);
        this.c_f.HttpPut(this.r_u.getUrl('level3.crm.ticket.operational.update_priority'), $(event.target).serialize()).subscribe(response => {
            if (response.isResponse) {
                this.c_f.ShowToast(response.message, 'success');
                this.LoadTicketInfo(this._ticket_id);
            } else {
                this.c_f.setLoading(false);
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    UpdateStatus(event) {
        this.c_f.setLoading(true);
        this.c_f.HttpPut(this.r_u.getUrl('level3.crm.ticket.operational.update_status'), $(event.target).serialize()).subscribe(response => {
            if (response.isResponse) {
                this.c_f.ShowToast(response.message, 'success');
                this.LoadTicketInfo(this._ticket_id);
            } else {
                this.c_f.setLoading(false);
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    DeleteItem(item) {
        this.index = item;
    }

    ConfirmedDeleteItem() {
        let url = this.r_u.getUrl('level3.crm.ticket.operational.remove_agent');
        this.c_f.HttpDelete(url + '/' + this.index.id).subscribe(response => {
            if (response.isResponse) {
                let index = this._ticket_detail.assigned_to.indexOf(this.index);
                this._ticket_detail.assigned_to.splice(index, 1);
                this.c_f.ShowToast(response.message, 'success');
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.CloseModel('delete-model-popup');
            this.c_f.setLoading(false);
        });
    }

    TicketPriority(val) {
        return PriorityList.getPriority('ticket.priority.' + val + '.val');
    }

    TicketPriorityColor(val) {
        return PriorityList.getPriority('ticket.priority.' + val + '.color');
    }

    TicketStatus(val) {
        return TicketStatusList.getStatus('ticket.status.' + val + '.val');
    }

    TicketStatusColor(val) {
        return TicketStatusList.getStatus('ticket.status.' + val + '.color');
    }

    GenerateName(data) {
        if (data) {
            let name = data.first_name;
            if (data.last_name) {
                name += ' ' + data.last_name;
            }
            return name;
        }
    }
}
