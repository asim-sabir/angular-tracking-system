import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CommonFunctions} from '../../../../../helpers/common.functions';
import {RequestUrls} from '../../../../../helpers/request.urls';
import {TicketCurd} from '../../../../../helpers/level-2/ticket/ticket-curd';
import {NotificationEmitter} from '../../../../../helpers/emitters/notification.emitter';
import {PriorityList} from '../../../../../helpers/priority.list';
import {OperationList} from '../../../../../helpers/operation.list';

declare let $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './add.component.html'
})
export class AddComponent implements OnInit {
    _client_list;
    _vehicle_list;
    _priority_list;
    _operational_list;
    type;

    constructor(private c_f: CommonFunctions,
                private r_u: RequestUrls,
                private n_e: NotificationEmitter,
                private router: Router) {
    }

    ngOnInit() {
        this.LoadData();
        this._priority_list = PriorityList.GetPlainPriorityList('ticket.priority');
        this._operational_list = OperationList.GetPlainOperationList('operational.ticket');
    }

    LoadData() {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level3.crm.ticket.operational.get_add');
        this.c_f.HttpGet(url).subscribe(response => {
            if (response.isResponse) {
                this._client_list = response.data.clients;
                this._vehicle_list = response.data.vehicles;
            } else {
                this.router.navigate(['/level-3/crm/operational/tickets']);
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    SaveTicket(event) {
        if (TicketCurd.ValidForm($(event.target))) {
            this.c_f.setLoading(true);
            this.c_f.HttpPost(this.r_u.getUrl('level3.crm.ticket.operational.post'), $(event.target).serialize()).subscribe(response => {
                if (response.isResponse) {
                    this.n_e.change(true);
                    this.router.navigate(['/level-3/crm/operational/tickets']);
                    this.c_f.ShowToast(response.message, 'success');
                } else {
                    this.c_f.setLoading(false);
                    this.c_f.ShowToast(response.message, 'error');
                }
                this.c_f.setLoading(false);
            });
        }
    }
}
