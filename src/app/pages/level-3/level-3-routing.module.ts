import {NgModule} from '@angular/core';
import {Level3Component} from './level-3.component';
import {Routes, RouterModule} from '@angular/router';
import {AuthGuard} from '../../_guards/auth.guard';
import {RouteGuard} from '../../_guards/route.guard';
import {PermissionsGuard} from '../../_guards/permissions.guard';

const routes: Routes = [
    {
        'path': 'level-3',
        'component': Level3Component,
        'canActivate': [
            AuthGuard,
            RouteGuard,
        ],
        'canActivateChild': [
            PermissionsGuard
        ],
        'children': [
            {
                path: 'sale',
                children: [
                    {
                        'path': 'index',
                        'loadChildren': './sale3/home/sale.home.module#SaleHomeModule'
                    },
                    // profile routes
                    {
                        'path': 'profile',
                        'loadChildren': './sale3/profile/view/view.module#ViewModule'
                    },
                    // client routes
                    {
                        'path': 'clients',
                        'loadChildren': './sale3/client/index/index.module#IndexModule'
                    },
                    {
                        'path': 'client/new/individual',
                        'loadChildren': './sale3/client/add/individual/add.module#AddModule'
                    },
                    {
                        'path': 'client/new/corporate',
                        'loadChildren': './sale3/client/add/corporate/add.module#AddModule'
                    },
                    {
                        'path': 'client/update/individual/:client_id',
                        'loadChildren': './sale3/client/update/individual/update.module#UpdateModule'
                    },
                    {
                        'path': 'client/update/corporate/:client_id',
                        'loadChildren': './sale3/client/update/corporate/update.module#UpdateModule'
                    },
                    {
                        'path': 'client/view/:client_id',
                        'loadChildren': './sale3/client/view/view.module#ViewModule'
                    },
                    // proposal routes
                    {
                        'path': 'proposals',
                        'loadChildren': './sale3/proposal/index/index.module#IndexModule'
                    },
                    {
                        'path': 'proposal/new',
                        'loadChildren': './sale3/proposal/add/add.module#AddModule'
                    },
                    // lead routes
                    {
                        'path': 'leads',
                        'loadChildren': './sale3/lead/index/index.module#IndexModule'
                    },
                    {
                        'path': 'lead/new',
                        'loadChildren': './sale3/lead/add/add.module#AddModule'
                    },
                    {
                        'path': 'lead/view/:lead_id',
                        'loadChildren': './sale3/lead/view/view.module#ViewModule'
                    },
                    // sale routes
                    {
                        'path': 'sales',
                        'loadChildren': './sale3/sale/index/index.module#IndexModule'
                    },
                    // report routes
                    {
                        'path': 'reports',
                        'loadChildren': './sale3/report/index/index.module#IndexModule'
                    },
                    // ticket routes
                    {
                        'path': 'tickets',
                        'loadChildren': './sale3/ticket/index/index.module#IndexModule'
                    },
                    {
                        'path': 'ticket/new',
                        'loadChildren': './sale3/ticket/add/add.module#AddModule'
                    },
                    {
                        'path': 'ticket/view/:ticket_id',
                        'loadChildren': './sale3/ticket/view/view.module#ViewModule'
                    }
                ]
            },
            // crm routes
            {
                path: 'crm',
                children: [
                    {
                        path: 'index',
                        loadChildren: './crm3/home/dashboard.module#DashboardModule'
                    },
                    // profile routes
                    {
                        'path': 'profile',
                        'loadChildren': './crm3/profile/view/view.module#ViewModule'
                    },
                    // client routes
                    {
                        'path': 'clients',
                        'loadChildren': './crm3/client/index/index.module#IndexModule'
                    },
                    {
                        'path': 'client/update/individual/:opt_id/:client_id',
                        'loadChildren': './crm3/client/update/individual/update.module#UpdateModule'
                    },
                    {
                        'path': 'client/update/corporate/:opt_id/:client_id',
                        'loadChildren': './crm3/client/update/corporate/update.module#UpdateModule'
                    },
                    {
                        'path': 'client/update/emergency-contact/:opt_id/:client_id',
                        'loadChildren': './crm3/client/update/e-contact/update.module#UpdateModule'
                    },
                    {
                        'path': 'client/update/vehicle/:opt_id/:vehicle_id',
                        'loadChildren': './crm3/client/update/vehicle/update.module#UpdateModule'
                    },
                    {
                        'path': 'client/view/:client_id',
                        'loadChildren': './crm3/client/view/view.module#ViewModule'
                    },
                    // lead routes
                    {
                        'path': 'leads',
                        'loadChildren': './crm3/lead/index/index.module#IndexModule'
                    },
                    {
                        'path': 'lead/update/:lead_id',
                        'loadChildren': './crm3/lead/update/update.module#UpdateModule'
                    },
                    // ticket routes
                    {
                        'path': 'tickets',
                        'loadChildren': './crm3/ticket/index/index.module#IndexModule'
                    },
                    {
                        'path': 'ticket/new',
                        'loadChildren': './crm3/ticket/add/add.module#AddModule'
                    },
                    {
                        'path': 'ticket/view/:ticket_id',
                        'loadChildren': './crm3/ticket/view/view.module#ViewModule'
                    },
                    // operational ticket routes
                    {
                        'path': 'operational/tickets',
                        'loadChildren': './crm3/operational-ticket/index/index.module#IndexModule'
                    },
                    {
                        'path': 'operational/ticket/new',
                        'loadChildren': './crm3/operational-ticket/add/add.module#AddModule'
                    },
                    {
                        'path': 'operational/ticket/view/:ticket_id',
                        'loadChildren': './crm3/operational-ticket/view/view.module#ViewModule'
                    },
                ]
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class Level3RoutingModule {
}