import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CommonFunctions} from '../../../../helpers/common.functions';
import {RequestUrls} from '../../../../helpers/request.urls';
import {CompanyCurd} from '../../../../helpers/super-admin/company/company-curd';

declare let $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './add.component.html'
})
export class AddComponent implements OnInit {

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private router: Router) {
    }

    ngOnInit() {
    }

    SaveCompany(event) {
        if (CompanyCurd.ValidForm($(event.target))) {
            this.c_f.setLoading(true);
            this.c_f.HttpPost(this.r_u.getUrl('level1.company.add'), $(event.target).serialize()).subscribe(response => {
                if (response.isResponse) {
                    this.router.navigate(['/level-1/company']);
                    this.c_f.ShowToast(response.message, 'success');
                } else {
                    this.c_f.setLoading(false);
                    this.c_f.ShowToast(response.message, 'error');
                }
                this.c_f.setLoading(false);
            });
        }
    }
}
