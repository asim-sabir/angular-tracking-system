import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CommonFunctions} from '../../../../helpers/common.functions';
import {RequestUrls} from '../../../../helpers/request.urls';

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './view.component.html',
})
export class ViewComponent implements OnInit {
    _role_detail;
    _permissions;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private route: ActivatedRoute, private r_c: Router) {
        this._role_detail = {};
    }

    ngOnInit() {
        // get route params
        this.route.params.subscribe(params => {
            this.LoadRoleInfo(params['role_id']);
        });
    }

    LoadRoleInfo(id) {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level1.role.get');
        this.c_f.HttpGet(url + '/' + id).subscribe(response => {
            if (response.isResponse) {
                this._role_detail = response.data.role;
                this._permissions = response.data.permissions;
                console.log(this._permissions)
            } else {
                this.c_f.ShowToast(response.message, 'error');
                this.r_c.navigate(['/level-1/role']);
            }
            this.c_f.setLoading(false);
        });
    }
}
