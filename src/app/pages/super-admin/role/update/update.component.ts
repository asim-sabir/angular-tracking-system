import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {CommonFunctions} from '../../../../helpers/common.functions';
import {RequestUrls} from '../../../../helpers/request.urls';
import {RoleCurd} from '../../../../helpers/super-admin/role/role-curd';

declare var $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './update.component.html'
})
export class UpdateComponent implements OnInit {
    _role_detail;
    _permission_list;
    _department_list;
    _page_list;
    private role_id;
    _list = [];
    private level = 0;
    private dept = 0;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private route: ActivatedRoute, private r_c: Router) {
        this._role_detail = {};
    }

    ngOnInit() {
        // get route params
        this.route.params.subscribe(params => {
            this.role_id = params['role_id'];
            this.LoadFormInfo(this.role_id);
        });
    }

    LoadFormInfo(id) {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level1.role.get_update');
        this.c_f.HttpGet(url + '/' + id).subscribe(response => {
            if (response.isResponse) {
                this._role_detail = response.data.role;
                this._permission_list = response.data.permissions;
                this._page_list = response.data.pages;
                this._department_list = response.data.department;
                this.ListedPermissions();
            } else {
                this.c_f.ShowToast(response.message, 'error');
                this.r_c.navigate(['/level-1/role']);
            }
            this.c_f.setLoading(false);
        });
    }

    LevelDept() {
        this.ListedPermissions();
    }

    ChangeDept() {
        this.ListedPermissions();
    }

    ListedPermissions() {
        let self = this;
        self._list = [];
        if (self._role_detail.level && self._role_detail.department) {
            self._page_list.forEach(function (val, key) {
                if (val['level'] == self._role_detail.level && val['department'] == self._role_detail.department) {
                    self._list.push(val);
                }
            });
        } else {
            self._list = [];
        }
    }

    SaveRole(event) {
        if (RoleCurd.ValidForm($(event.target))) {
            this.c_f.setLoading(true);
            this.c_f.HttpPut(this.r_u.getUrl('level1.role.put'), $(event.target).serialize()).subscribe(response => {
                if (response.isResponse) {
                    this.r_c.navigate(['/level-1/role']);
                    this.c_f.ShowToast(response.message, 'success');
                } else {
                    this.c_f.setLoading(false);
                    this.c_f.ShowToast(response.message, 'error');
                }
                this.c_f.setLoading(false);
            });
        }
    }

    CheckPermission(all_permissions, permission) {
        if (all_permissions[permission] === 0 || all_permissions[permission]) {
            return true;
        }
        return false;
    }
}
