import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CommonFunctions} from '../../../../helpers/common.functions';
import {RequestUrls} from '../../../../helpers/request.urls';
import {RoleCurd} from '../../../../helpers/super-admin/role/role-curd';

declare let $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './add.component.html'
})
export class AddComponent implements OnInit {
    _page_list;
    _department_list;
    _list = [];
    private level = 0;
    private dept = 0;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private router: Router) {
    }

    ngOnInit() {
        this.LoadFormData();
    }

    LoadFormData() {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level1.role.get_add');
        this.c_f.HttpGet(url).subscribe(response => {
            if (response.isResponse) {
                this._page_list = response.data.pages;
                this._department_list = response.data.department;
            } else {
                this.c_f.ShowToast(response.message, 'error');
                this.router.navigate(['/level-1/role']);
            }
            this.c_f.setLoading(false);
        });
    }

    LevelDept(event) {
        this.level = event;
        this.ListedPermissions();
    }

    ChangeDept(event) {
        this.dept = event;
        this.ListedPermissions();
    }

    ListedPermissions() {
        let self = this;
        self._list = [];
        if (this.level && this.dept) {
            this._page_list.forEach(function (val, key) {
                if (val['level'] == self.level && val['department'] == self.dept) {
                    self._list.push(val);
                }
            });
        } else {
            this._list = [];
        }
    }

    SaveRole(event) {
        if (RoleCurd.ValidForm($(event.target))) {
            this.c_f.setLoading(true);
            this.c_f.HttpPost(this.r_u.getUrl('level1.role.post'), $(event.target).serialize()).subscribe(response => {
                if (response.isResponse) {
                    this.router.navigate(['/level-1/role']);
                    this.c_f.ShowToast(response.message, 'success');
                } else {
                    this.c_f.setLoading(false);
                    this.c_f.ShowToast(response.message, 'error');
                }
                this.c_f.setLoading(false);
            });
        }
    }
}
