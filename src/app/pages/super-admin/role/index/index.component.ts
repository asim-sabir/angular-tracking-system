import {Component, OnInit} from '@angular/core';
import {CommonFunctions} from '../../../../helpers/common.functions';
import {RequestUrls} from '../../../../helpers/request.urls';
import {PushNotificationService} from 'ng-push-notification';

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './index.component.html'
})
export class IndexComponent implements OnInit {
    _roles_list;
    filter;
    private index;
    constructor(private c_f: CommonFunctions, private r_u: RequestUrls) {
    }

    ngOnInit() {
        this.LoadRolesData();
    }

    LoadRolesData() {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level1.role.index');
        this.c_f.HttpGet(url).subscribe(response => {
            if (response.isResponse) {
                this._roles_list = response.data;
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    DeleteItem(item) {
        this.index = item;
    }

    ConfirmedDeleteItem() {
        let url = this.r_u.getUrl('level1.role.delete');
        this.c_f.HttpDelete(url + '/' + this.index.id).subscribe(response => {
            if (response.isResponse) {
                let index = this._roles_list.indexOf(this.index);
                this._roles_list.splice(index, 1);
                this.c_f.ShowToast(response.message, 'success');
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.CloseModel('delete-model-popup');
            this.c_f.setLoading(false);
        });
    }
}
