import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CommonFunctions} from '../../../../helpers/common.functions';
import {RequestUrls} from '../../../../helpers/request.urls';
import {ProductCurd} from '../../../../helpers/super-admin/product/product-curd';

declare let $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './add.component.html'
})
export class AddComponent implements OnInit {
    _company_list;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private router: Router) {
    }

    ngOnInit() {
        this.LoadCompanyData();
    }

    LoadCompanyData() {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level1.company.index');
        this.c_f.HttpGet(url).subscribe(response => {
            if (response.isResponse) {
                this._company_list = response.data;
            } else {
                this.router.navigate(['/level-1/product']);
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    SaveProduct(event) {
        if (ProductCurd.ValidForm($(event.target))) {
            this.c_f.setLoading(true);
            this.c_f.HttpPost(this.r_u.getUrl('level1.product.post'), $(event.target).serialize()).subscribe(response => {
                if (response.isResponse) {
                    this.router.navigate(['/level-1/product']);
                    this.c_f.ShowToast(response.message, 'success');
                } else {
                    this.c_f.setLoading(false);
                    this.c_f.ShowToast(response.message, 'error');
                }
                this.c_f.setLoading(false);
            });
        }
    }
}
