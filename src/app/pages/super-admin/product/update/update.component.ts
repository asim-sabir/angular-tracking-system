import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {CommonFunctions} from '../../../../helpers/common.functions';
import {RequestUrls} from '../../../../helpers/request.urls';
import {ProductCurd} from '../../../../helpers/super-admin/product/product-curd';

declare var $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './update.component.html'
})
export class UpdateComponent implements OnInit {
    _product_detail;
    _company_list;
    private product_id;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private route: ActivatedRoute, private r_c: Router) {
        this._product_detail = {};
    }

    ngOnInit() {
        // get route params
        this.route.params.subscribe(params => {
            this.product_id = params['product_id'];
            this.LoadProductInfo(this.product_id);
        });
    }

    LoadProductInfo(id) {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level1.product.get');
        this.c_f.HttpGet(url + '/' + id).subscribe(response => {
            if (response.isResponse) {
                this._product_detail = response.data.product_info;
                this._company_list = response.data.companies;
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    SaveProduct(event) {
        if (ProductCurd.ValidForm($(event.target))) {
            this.c_f.setLoading(true);
            this.c_f.HttpPut(this.r_u.getUrl('level1.product.put'), $(event.target).serialize()).subscribe(response => {
                if (response.isResponse) {
                    this.r_c.navigate(['/level-1/product']);
                    this.c_f.ShowToast(response.message, 'success');
                } else {
                    this.c_f.setLoading(false);
                    this.c_f.ShowToast(response.message, 'error');
                }
                this.c_f.setLoading(false);
            });
        }
    }
}
