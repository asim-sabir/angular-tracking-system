import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {CommonFunctions} from '../../../../helpers/common.functions';
import {RequestUrls} from '../../../../helpers/request.urls';
import {PlanCurd} from '../../../../helpers/super-admin/plan/plan-curd';

declare var $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './update.component.html'
})
export class UpdateComponent implements OnInit {
    _package_detail;
    _product_list;
    _feature_list;
    private plan_id;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private route: ActivatedRoute, private r_c: Router) {
        this._package_detail = {};
    }

    ngOnInit() {
        // get route params
        this.route.params.subscribe(params => {
            this.plan_id = params['plan_id'];
            this.LoadPackageInfo(this.plan_id);
        });
        PlanCurd.init();
    }

    LoadPackageInfo(id) {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level1.package.get_update');
        this.c_f.HttpGet(url + '/' + id).subscribe(response => {
            if (response.isResponse) {
                this._package_detail = response.data.package;
                this._product_list = response.data.products;
                this._feature_list = response.data.features;
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    SavePlan(event) {
        if (PlanCurd.ValidForm($(event.target))) {
            this.c_f.setLoading(true);
            this.c_f.HttpPut(this.r_u.getUrl('level1.package.put'), $(event.target).serialize()).subscribe(response => {
                if (response.isResponse) {
                    this.r_c.navigate(['/level-1/plan']);
                    this.c_f.ShowToast(response.message, 'success');
                } else {
                    this.c_f.setLoading(false);
                    this.c_f.ShowToast(response.message, 'error');
                }
                this.c_f.setLoading(false);
            });
        }
    }
}
