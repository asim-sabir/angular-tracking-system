import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CommonFunctions} from '../../../../helpers/common.functions';
import {RequestUrls} from '../../../../helpers/request.urls';

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './view.component.html',
})
export class ViewComponent implements OnInit {
    _package_detail;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private route: ActivatedRoute, private r_c: Router) {
        this._package_detail = {};
    }

    ngOnInit() {
        // get route params
        this.route.params.subscribe(params => {
            this.LoadPackageInfo(params['plan_id']);
        });
    }

    LoadPackageInfo(id) {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level1.package.get');
        this.c_f.HttpGet(url + '/' + id).subscribe(response => {
            if (response.isResponse) {
                this._package_detail = response.data;
            } else {
                this.c_f.ShowToast(response.message, 'error');
                this.r_c.navigate(['/level-1/plan']);
            }
            this.c_f.setLoading(false);
        });
    }
}
