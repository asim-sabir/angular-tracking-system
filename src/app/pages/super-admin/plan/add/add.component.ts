import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CommonFunctions} from '../../../../helpers/common.functions';
import {RequestUrls} from '../../../../helpers/request.urls';
import {PlanCurd} from '../../../../helpers/super-admin/plan/plan-curd';

declare let $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './add.component.html'
})
export class AddComponent implements OnInit {
    _product_list;
    _feature_list;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private router: Router) {
    }

    ngOnInit() {
        this.LoadFormData();
        PlanCurd.init();
    }

    LoadFormData() {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level1.package.get_add');
        this.c_f.HttpGet(url).subscribe(response => {
            if (response.isResponse) {
                this._product_list = response.data.products;
                this._feature_list = response.data.features;
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    SavePlan(event) {
        if (PlanCurd.ValidForm($(event.target))) {
            this.c_f.setLoading(true);
            this.c_f.HttpPost(this.r_u.getUrl('level1.package.post'), $(event.target).serialize()).subscribe(response => {
                if (response.isResponse) {
                    this.router.navigate(['/level-1/plan']);
                    this.c_f.ShowToast(response.message, 'success');
                } else {
                    this.c_f.setLoading(false);
                    this.c_f.ShowToast(response.message, 'error');
                }
                this.c_f.setLoading(false);
            });
        }
    }
}
