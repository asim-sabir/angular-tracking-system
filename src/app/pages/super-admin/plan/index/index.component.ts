import {Component, OnInit} from '@angular/core';
import {CommonFunctions} from '../../../../helpers/common.functions';
import {RequestUrls} from '../../../../helpers/request.urls';

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './index.component.html'
})
export class IndexComponent implements OnInit {
    _packages_list;
    filter;
    private index;
    constructor(private c_f: CommonFunctions, private r_u: RequestUrls) {
    }

    ngOnInit() {
        this.LoadPackageData();
    }

    LoadPackageData() {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level1.package.index');
        this.c_f.HttpGet(url).subscribe(response => {
            if (response.isResponse) {
                this._packages_list = response.data;
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    DeleteItem(item) {
        this.index = item;
    }

    ConfirmedDeleteItem() {
        let url = this.r_u.getUrl('level1.package.delete');
        this.c_f.HttpDelete(url + '/' + this.index.package_id).subscribe(response => {
            if (response.isResponse) {
                let index = this._packages_list.indexOf(this.index);
                this._packages_list.splice(index, 1);
                this.c_f.ShowToast(response.message, 'success');
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.CloseModel('delete-model-popup');
            this.c_f.setLoading(false);
        });
    }
}
