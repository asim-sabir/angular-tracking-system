import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {DefaultComponent} from '../../../layouts/super-admin/default/default.component';
import {DashboardComponent} from './dashboard.component';
import {SuperAdminLayoutModule} from '../../../layouts/super-admin/super-admin-layout.module';

const routes: Routes = [
    {
        'path': '',
        'component': DefaultComponent,
        children: [
            {
                path: '',
                component: DashboardComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), SuperAdminLayoutModule
    ], exports: [RouterModule],
    declarations: [DashboardComponent]
})
export class DashboardModule {
}
