import {Component, OnInit} from '@angular/core';
import {CommonFunctions} from '../../../../helpers/common.functions';
import {RequestUrls} from '../../../../helpers/request.urls';

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './index.component.html'
})
export class IndexComponent implements OnInit {
    _features_list;
    filter;
    private index;
    constructor(private c_f: CommonFunctions, private r_u: RequestUrls) {
    }

    ngOnInit() {
        this.LoadProductData();
    }

    LoadProductData() {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level1.feature.index');
        this.c_f.HttpGet(url).subscribe(response => {
            if (response.isResponse) {
                this._features_list = response.data;
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    DeleteItem(item) {
        this.index = item;
    }

    ConfirmedDeleteItem() {
        let url = this.r_u.getUrl('level1.feature.delete');
        this.c_f.HttpDelete(url + '/' + this.index.feature_id).subscribe(response => {
            if (response.isResponse) {
                let index = this._features_list.indexOf(this.index);
                this._features_list.splice(index, 1);
                this.c_f.ShowToast(response.message, 'success');
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.CloseModel('delete-model-popup');
            this.c_f.setLoading(false);
        });
    }
}
