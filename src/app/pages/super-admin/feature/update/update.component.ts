import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {CommonFunctions} from '../../../../helpers/common.functions';
import {RequestUrls} from '../../../../helpers/request.urls';
import {FeatureCurd} from '../../../../helpers/super-admin/feature/feature-curd';

declare var $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './update.component.html'
})
export class UpdateComponent implements OnInit {
    _feature_detail;
    private feature_id;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private route: ActivatedRoute, private r_c: Router) {
        this._feature_detail = {};
    }

    ngOnInit() {
        // get route params
        this.route.params.subscribe(params => {
            this.feature_id = params['feature_id'];
            this.LoadFeatureInfo(this.feature_id);
        });
    }

    LoadFeatureInfo(id) {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level1.feature.get');
        this.c_f.HttpGet(url + '/' + id).subscribe(response => {
            if (response.isResponse) {
                this._feature_detail = response.data;
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }

    SaveFeature(event) {
        if (FeatureCurd.ValidForm($(event.target))) {
            this.c_f.setLoading(true);
            this.c_f.HttpPut(this.r_u.getUrl('level1.feature.put'), $(event.target).serialize()).subscribe(response => {
                if (response.isResponse) {
                    this.r_c.navigate(['/level-1/feature']);
                    this.c_f.ShowToast(response.message, 'success');
                } else {
                    this.c_f.setLoading(false);
                    this.c_f.ShowToast(response.message, 'error');
                }
                this.c_f.setLoading(false);
            });
        }
    }
}
