import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {DefaultComponent} from '../../../../layouts/super-admin/default/default.component';
import {SuperAdminLayoutModule} from '../../../../layouts/super-admin/super-admin-layout.module';
import {ViewComponent} from './view.component';

const routes: Routes = [
    {
        path: '',
        component: DefaultComponent,
        children: [
            {
                path: '',
                component: ViewComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), SuperAdminLayoutModule
    ], exports: [
        RouterModule
    ],
    declarations: [ViewComponent]
})
export class ViewModule { }
