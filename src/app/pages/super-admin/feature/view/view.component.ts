import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {CommonFunctions} from '../../../../helpers/common.functions';
import {RequestUrls} from '../../../../helpers/request.urls';

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './view.component.html',
})
export class ViewComponent implements OnInit {
    _feature_detail;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private route: ActivatedRoute) {
        this._feature_detail = {};
    }

    ngOnInit() {
        // get route params
        this.route.params.subscribe(params => {
            this.LoadFeatureInfo(params['feature_id']);
        });
    }

    LoadFeatureInfo(id) {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level1.feature.get');
        this.c_f.HttpGet(url + '/' + id).subscribe(response => {
            if (response.isResponse) {
                this._feature_detail = response.data;
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }
}
