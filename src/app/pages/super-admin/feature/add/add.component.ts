import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CommonFunctions} from '../../../../helpers/common.functions';
import {RequestUrls} from '../../../../helpers/request.urls';
import {FeatureCurd} from '../../../../helpers/super-admin/feature/feature-curd';

declare let $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './add.component.html'
})
export class AddComponent implements OnInit {

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private router: Router) {
    }

    ngOnInit() {
    }

    SaveFeature(event) {
        if (FeatureCurd.ValidForm($(event.target))) {
            this.c_f.setLoading(true);
            this.c_f.HttpPost(this.r_u.getUrl('level1.feature.post'), $(event.target).serialize()).subscribe(response => {
                if (response.isResponse) {
                    this.router.navigate(['/level-1/feature']);
                    this.c_f.ShowToast(response.message, 'success');
                } else {
                    this.c_f.setLoading(false);
                    this.c_f.ShowToast(response.message, 'error');
                }
                this.c_f.setLoading(false);
            });
        }
    }
}
