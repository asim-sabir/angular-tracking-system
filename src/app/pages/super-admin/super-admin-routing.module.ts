import {NgModule} from '@angular/core';
import {SuperAdminComponent} from './super-admin.component';
import {Routes, RouterModule} from '@angular/router';
import {AuthGuard} from '../../_guards/auth.guard';
import {RouteGuard} from '../../_guards/route.guard';
import {PermissionsGuard} from '../../_guards/permissions.guard';

const routes: Routes = [
    {
        'path': 'level-1',
        'component': SuperAdminComponent,
        'canActivate': [
            AuthGuard,
            RouteGuard,
        ],
        'canActivateChild': [
            PermissionsGuard
        ],
        'children': [
            {
                'path': 'dashboard',
                'loadChildren': './dashboard/dashboard.module#DashboardModule'
            },
            // company routes
            {
                'path': 'company',
                'loadChildren': './company/index/index.module#IndexModule'
            }, {
                'path': 'company/new-company',
                'loadChildren': './company/add/add.module#AddModule'
            }, {
                'path': 'company/view-company/:company_id',
                'loadChildren': './company/view/view.module#ViewModule'
            }, {
                'path': 'company/update-company/:company_id',
                'loadChildren': './company/update/update.module#UpdateModule'
            },
            // product routes
            {
                'path': 'product',
                'loadChildren': './product/index/index.module#IndexModule'
            }, {
                'path': 'product/new-product',
                'loadChildren': './product/add/add.module#AddModule'
            }, {
                'path': 'product/view-product/:product_id',
                'loadChildren': './product/view/view.module#ViewModule'
            }, {
                'path': 'product/update-product/:product_id',
                'loadChildren': './product/update/update.module#UpdateModule'
            },
            // feature routes
            {
                'path': 'feature',
                'loadChildren': './feature/index/index.module#IndexModule'
            }, {
                'path': 'feature/new-feature',
                'loadChildren': './feature/add/add.module#AddModule'
            }, {
                'path': 'feature/view-feature/:feature_id',
                'loadChildren': './feature/view/view.module#ViewModule'
            }, {
                'path': 'feature/update-feature/:feature_id',
                'loadChildren': './feature/update/update.module#UpdateModule'
            },
            // package routes
            {
                'path': 'plan',
                'loadChildren': './plan/index/index.module#IndexModule'
            }, {
                'path': 'plan/new-plan',
                'loadChildren': './plan/add/add.module#AddModule'
            }, {
                'path': 'plan/view-plan/:plan_id',
                'loadChildren': './plan/view/view.module#ViewModule'
            }, {
                'path': 'plan/update-plan/:plan_id',
                'loadChildren': './plan/update/update.module#UpdateModule'
            },
            // user routes
            {
                'path': 'user',
                'loadChildren': './user/index/index.module#IndexModule'
            }, {
                'path': 'user/new-user',
                'loadChildren': './user/add/add.module#AddModule'
            }, {
                'path': 'user/view-user/:user_id',
                'loadChildren': './user/view/view.module#ViewModule'
            }, {
                'path': 'user/update-user/:user_id',
                'loadChildren': './user/update/update.module#UpdateModule'
            },
            // role routes
            {
                'path': 'role',
                'loadChildren': './role/index/index.module#IndexModule'
            }, {
                'path': 'role/new-role',
                'loadChildren': './role/add/add.module#AddModule'
            }, {
                'path': 'role/view-role/:role_id',
                'loadChildren': './role/view/view.module#ViewModule'
            }, {
                'path': 'role/update-role/:role_id',
                'loadChildren': './role/update/update.module#UpdateModule'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SuperAdminRoutingModule {
}