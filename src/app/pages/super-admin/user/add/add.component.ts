import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CommonFunctions} from '../../../../helpers/common.functions';
import {RequestUrls} from '../../../../helpers/request.urls';
import {UserCurd} from '../../../../helpers/super-admin/user/user-curd';

declare let $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './add.component.html'
})
export class AddComponent implements OnInit {
    _role_list;
    _department_list;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private router: Router) {
    }

    ngOnInit() {
        this.LoadFormData();
    }

    LoadFormData() {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level1.user.get_add');
        this.c_f.HttpGet(url).subscribe(response => {
            if (response.isResponse) {
                this._role_list = response.data.roles;
                this._department_list = response.data.department;
            } else {
                this.c_f.ShowToast(response.message, 'error');
                this.router.navigate(['/level-1/user']);
            }
            this.c_f.setLoading(false);
        });
    }

    SaveUser(event) {
        if (UserCurd.ValidForm($(event.target))) {
            this.c_f.setLoading(true);
            this.c_f.HttpPost(this.r_u.getUrl('level1.user.post'), $(event.target).serialize()).subscribe(response => {
                if (response.isResponse) {
                    this.router.navigate(['/level-1/user']);
                    this.c_f.ShowToast(response.message, 'success');
                } else {
                    this.c_f.setLoading(false);
                    this.c_f.ShowToast(response.message, 'error');
                }
                this.c_f.setLoading(false);
            });
        }
    }
}
