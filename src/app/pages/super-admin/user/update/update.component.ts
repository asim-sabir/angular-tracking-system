import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {CommonFunctions} from '../../../../helpers/common.functions';
import {RequestUrls} from '../../../../helpers/request.urls';
import {UserCurd} from '../../../../helpers/super-admin/user/user-curd';


declare var $: any;

@Component({
    selector: '.m-grid__item.m-grid__item--fluid.m-wrapper',
    templateUrl: './update.component.html'
})
export class UpdateComponent implements OnInit {
    _user_detail;
    _role_list;
    _department_list;
    private user_id;

    constructor(private c_f: CommonFunctions, private r_u: RequestUrls, private route: ActivatedRoute, private r_c: Router) {
        this._user_detail = {};
    }

    ngOnInit() {
        // get route params
        this.route.params.subscribe(params => {
            this.user_id = params['user_id'];
            this.LoadUserInfo(this.user_id);
        });
    }

    LoadUserInfo(id) {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('level1.user.get_update');
        this.c_f.HttpGet(url + '/' + id).subscribe(response => {
            if (response.isResponse) {
                this._user_detail = response.data.user_info;
                this._role_list = response.data.roles;
                this._department_list = response.data.department;
            } else {
                this.c_f.ShowToast(response.message, 'error');
                this.r_c.navigate(['/level-1/user']);
            }
            this.c_f.setLoading(false);
        });
    }

    SaveUser(event) {
        if (UserCurd.ValidForm($(event.target))) {
            this.c_f.setLoading(true);
            this.c_f.HttpPut(this.r_u.getUrl('level1.user.put'), $(event.target).serialize()).subscribe(response => {
                if (response.isResponse) {
                    this.r_c.navigate(['/level-1/user']);
                    this.c_f.ShowToast(response.message, 'success');
                } else {
                    this.c_f.setLoading(false);
                    this.c_f.ShowToast(response.message, 'error');
                }
                this.c_f.setLoading(false);
            });
        }
    }
}
