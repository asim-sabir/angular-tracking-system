import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {DefaultComponent} from '../../../../layouts/super-admin/default/default.component';
import {IndexComponent} from './index.component';
import {SuperAdminLayoutModule} from '../../../../layouts/super-admin/super-admin-layout.module';

const routes: Routes = [
    {
        'path': '',
        'component': DefaultComponent,
        'children': [
            {
                'path': '',
                'component': IndexComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), SuperAdminLayoutModule
    ], exports: [RouterModule],
    declarations: [IndexComponent]
})
export class IndexModule {
}
