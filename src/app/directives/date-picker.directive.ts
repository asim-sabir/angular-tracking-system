import {Directive, ElementRef, AfterViewInit} from '@angular/core';
declare var $: any;
@Directive({
    selector: '[appDatePicker]'
})
export class DatePickerDirective implements AfterViewInit {
    private _element: any;

    constructor(el: ElementRef) {
        this._element = $(el.nativeElement);
    }

    ngAfterViewInit() {
        this._element.datepicker();
    }
}
