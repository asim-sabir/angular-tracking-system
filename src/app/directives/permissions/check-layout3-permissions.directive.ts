import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';
import {DataSharing} from '../../helpers/data.sharing';
import {Helper} from '../../helpers/helper';

@Directive({
    selector: '[appCheckLayout3Permissions]'
})
export class CheckLayout3PermissionsDirective {

    constructor(private templateRef: TemplateRef<any>, private viewContainer: ViewContainerRef) {
    }

    @Input()
    set appCheckLayout3Permissions(permission: string) {
        if (DataSharing.user_role() === 'Admin') {
            this.viewContainer.createEmbeddedView(this.templateRef);
        } else {
            if (Helper.CheckPermissionExist(permission)) {
                // If condition is true add template to DOM
                this.viewContainer.createEmbeddedView(this.templateRef);
            } else {
                // Else remove template from DOM
                this.viewContainer.clear();
            }
        }
    }

}