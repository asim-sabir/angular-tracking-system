import {Directive, ElementRef, AfterViewInit, EventEmitter, Output, Input} from '@angular/core';

declare var $: any;

@Directive({
    selector: '[appSelectorTwo]'
})
export class SelectorTwoDirective implements AfterViewInit {
    @Input() value: any;
    @Output() onChange = new EventEmitter<string[]>();
    public _element: any;

    constructor(private el: ElementRef) {
        this._element = $(el.nativeElement);
    }

    ngAfterViewInit() {
        this.el.nativeElement.style.width = '100%';
        this._element.select2();
        this._element.val(this.value).trigger('change');
        this._element.on('change', (e: any) => {
            this.onChange.emit($(e.target).val());
        });
    }

}
