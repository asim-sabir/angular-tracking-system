﻿import {Injectable} from '@angular/core';
import {Location} from '@angular/common';
import {Router, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {DataSharing} from '../helpers/data.sharing';
import {Permissions} from '../helpers/permissions';
import {CommonFunctions} from '../helpers/common.functions';

@Injectable()
export class PermissionsGuard implements CanActivateChild {
    constructor(private router: Router, private c_f: CommonFunctions, private _location: Location) {
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let info = DataSharing.GetJsonObj('info');
        if (info.roles[0].name === 'Admin') {
            return true;
        }
        if (route.url.length > 0) {
            if (Permissions.CheckRoute(state.url, route)) {
                return true;
            } else {
                this.c_f.ShowToast('You don\'t permissions to access this', 'error');
                return false;
            }
        }
        return true;
    }
}
