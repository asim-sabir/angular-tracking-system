﻿import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {SheetsLoader} from '../helpers/sheets-loader';
import {DataSharing} from '../helpers/data.sharing';
import {CommonFunctions} from '../helpers/common.functions';

@Injectable()
export class RouteGuard implements CanActivate {
    constructor(private router: Router, private c_f: CommonFunctions) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        try {
            let info = DataSharing.GetJsonObj('info');
            let parent_route = route.routeConfig.path;
            SheetsLoader.Level3Sheets(parent_route);
            // now check user level
            if (info.level === parent_route) {
                return true;
            } else {
                DataSharing.clearAll();
                this.c_f.ShowToast('You don\'t permissions to access this', 'error');
                this.router.navigate(['login'], {queryParams: {returnUrl: state.url}});
                return false;
            }
        } catch (e) {
            this.router.navigate(['login'], {queryParams: {returnUrl: state.url}});
            return false;
        }
    }
}
