import {NgModule} from '@angular/core';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {AsideNavComponent} from './aside-nav/aside-nav.component';
import {DefaultComponent} from './default/default.component';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
    declarations: [
        HeaderComponent,
        AsideNavComponent,
        FooterComponent,
        DefaultComponent,
    ],
    exports: [
        HeaderComponent,
        AsideNavComponent,
        FooterComponent,
        DefaultComponent,
        SharedModule
    ],
    imports: [
        CommonModule,
        RouterModule,

    ]
})
export class SuperAdminLayoutModule {
}