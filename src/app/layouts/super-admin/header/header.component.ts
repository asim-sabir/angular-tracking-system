import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {RequestUrls} from '../../../helpers/request.urls';
import {CommonFunctions} from '../../../helpers/common.functions';
import {DataSharing} from '../../../helpers/data.sharing';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {
    _name = '';
    _email = '';

    constructor(private r_u: RequestUrls, private c_f: CommonFunctions, private r_c: Router) {

    }

    ngOnInit() {
    }

    UserInfoPanel() {
        let user_info = DataSharing.RetriveJsonObj('info');
        this._name = user_info.first_name;
        if (user_info.last_name) {
            this._name += ' ' + user_info.last_name;

        }
        this._email = user_info.email;
    }

    Logout() {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('auth.logout');
        this.c_f.HttpGet(url).subscribe(response => {
            if (response.isResponse) {
                DataSharing.clearAll();
                this.r_c.navigate(['login']);
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }
}