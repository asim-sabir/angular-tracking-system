import {Component, OnInit, AfterViewInit} from '@angular/core';
import {Router} from '@angular/router';
import {DataSharing} from '../../../helpers/data.sharing';
import {RequestUrls} from '../../../helpers/request.urls';
import {CommonFunctions} from '../../../helpers/common.functions';
import {Emitters} from '../../../helpers/emitters/emitters';
import {NotificationEmitter} from '../../../helpers/emitters/notification.emitter';
import {Helper} from '../../../helpers/helper';

declare let mLayout: any;

@Component({
    selector: 'app-level-3-header',
    templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, AfterViewInit {
    _name = '';
    _email = '';
    _dept = '';
    user_info;
    _header_info;

    constructor(private e_c: Emitters,
                private n_e: NotificationEmitter,
                private r_u: RequestUrls,
                private c_f: CommonFunctions,
                private r_c: Router) {

        this.user_info = DataSharing.RetriveJsonObj('info');
        this._header_info = {};
    }

    ngOnInit() {
        this.UserInfoPanel();
        this.GetData();
        this.e_c.getEmittedValue().subscribe(item => {
            this.user_info = item;
            this.UserInfoPanel();
        });
        this.n_e.getEmittedValue().subscribe(item => {
            if (item) {
                this.GetData();
            }
        });
    }

    ngAfterViewInit() {
        mLayout.initHeader();
    }
    UserInfoPanel() {
        this._name = this.user_info.first_name;
        if (this.user_info.last_name) {
            this._name += ' ' + this.user_info.last_name;

        }
        this._email = this.user_info.email;
        this._dept = DataSharing.user_dept();
    }

    GetData() {
        let url = this.r_u.getUrl('level3.' + this._dept + '.header.get');
        this.c_f.HttpGet(url).subscribe(response => {
            if (response.isResponse) {
                this._header_info = response.data;
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
        });
    }

    Logout() {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('auth.logout');
        this.c_f.HttpGet(url).subscribe(response => {
            if (response.isResponse) {
                DataSharing.clearAll();
                this.r_c.navigate(['login']);
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }
}