import {NgModule} from '@angular/core';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {DefaultComponent} from './default/default.component';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {CheckLayout3PermissionsDirective} from '../../directives/permissions/check-layout3-permissions.directive';

@NgModule({
    declarations: [
        HeaderComponent,
        FooterComponent,
        DefaultComponent,
        CheckLayout3PermissionsDirective
    ],
    exports: [
        HeaderComponent,
        FooterComponent,
        DefaultComponent,
        SharedModule,
        CheckLayout3PermissionsDirective
    ],
    imports: [
        CommonModule,
        RouterModule,

    ]
})
export class Level3LayoutModule {
}