import {Component, OnInit, ViewEncapsulation, AfterViewInit} from '@angular/core';

declare let mLayout: any;
declare let $: any;

@Component({
    selector: 'app-aside-nav-level-2',
    templateUrl: './aside-nav.component.html',
    encapsulation: ViewEncapsulation.None,
})
export class AsideNavComponent implements OnInit, AfterViewInit {


    constructor() {

    }

    ngOnInit() {

    }

    ngAfterViewInit() {

        mLayout.initAside();
        let menu = mLayout.getAsideMenu();
        let item = $(menu).find('a[href="' + window.location.pathname + '"]').parent('.m-menu__item');
        (<any>$(menu).data('menu')).setActiveItem(item);
    }

}