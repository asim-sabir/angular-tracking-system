import {NgModule} from '@angular/core';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {AsideNavComponent} from './aside-nav/aside-nav.component';
import {DefaultComponent} from './default/default.component';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {CheckLayout2PermissionsDirective} from '../../directives/permissions/check-layout2-permissions.directive';

@NgModule({
    declarations: [
        HeaderComponent,
        AsideNavComponent,
        FooterComponent,
        DefaultComponent,
        CheckLayout2PermissionsDirective
    ],
    exports: [
        HeaderComponent,
        AsideNavComponent,
        FooterComponent,
        DefaultComponent,
        SharedModule,
        CheckLayout2PermissionsDirective
    ],
    imports: [
        CommonModule,
        RouterModule,

    ]
})
export class Level2LayoutModule {
}