import {Component, OnInit, AfterViewInit} from '@angular/core';
import {Router} from '@angular/router';
import {NotificationEmitter} from '../../../helpers/emitters/notification.emitter';
import {RequestUrls} from '../../../helpers/request.urls';
import {CommonFunctions} from '../../../helpers/common.functions';
import {DataSharing} from '../../../helpers/data.sharing';

declare let mLayout: any;

@Component({
    selector: 'app-header-level-2',
    templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit, AfterViewInit {
    _name = '';
    _email = '';
    _dept = '';
    _header_info;

    constructor(private r_u: RequestUrls, private c_f: CommonFunctions, private r_c: Router, private n_e: NotificationEmitter) {
        this._header_info = {};
    }

    ngOnInit() {
        this._dept = DataSharing.user_dept();
        this.GetData();
        this.n_e.getEmittedValue().subscribe(item => {
            if (item) {
                this.GetData();
            }
            this.UserInfoPanel();
        });
    }

    ngAfterViewInit() {
        mLayout.initHeader();
    }

    UserInfoPanel() {
        let user_info = DataSharing.RetriveJsonObj('info');
        this._name = user_info.first_name;
        if (user_info.last_name) {
            this._name += ' ' + user_info.last_name;

        }
        this._email = user_info.email;
        this._dept = DataSharing.user_dept();
    }

    GetData() {
        let url = this.r_u.getUrl('level2.hod.' + this._dept + '.header.get');
        this.c_f.HttpGet(url).subscribe(response => {
            if (response.isResponse) {
                this._header_info = response.data;
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
        });
    }

    Logout() {
        this.c_f.setLoading(true);
        let url = this.r_u.getUrl('auth.logout');
        this.c_f.HttpGet(url).subscribe(response => {
            if (response.isResponse) {
                DataSharing.clearAll();
                this.r_c.navigate(['login']);
            } else {
                this.c_f.ShowToast(response.message, 'error');
            }
            this.c_f.setLoading(false);
        });
    }
}