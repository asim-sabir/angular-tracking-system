import {Component, OnInit} from '@angular/core';
import {NotificationEmitter} from './helpers/emitters/notification.emitter';
import {NotificationService} from './services/socket/notification.service';
import {PushNotificationService} from 'ng-push-notification';
import {DataSharing} from './helpers/data.sharing';

@Component({
    selector: 'body',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    constructor(private pushNotification: PushNotificationService, private n_s: NotificationService,
                private n_e: NotificationEmitter) {

    }

    ngOnInit() {
        this.n_s.getMessage('global-channel:App\\Events\\ClientNotification').subscribe(item => {
            if (item) {
                this.showPush(item);
            }
        });
    }

    showPush(item) {
        console.log(item)
        if (item.data.dept_id == DataSharing.user_dept_id() && item.data.job_post == DataSharing.user_post()) {
            this.pushNotification.show(item.data.message);
            this.n_e.change(true);
        }
    }
}
