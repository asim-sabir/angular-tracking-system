import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {NotificationSocket} from '../../helpers/socket/notification.socket';

@Injectable()
export class NotificationService extends NotificationSocket {

    sendMessage(event_name: string, data: any) {
        this.emit(event_name, data);
    }

    getMessage(event_name: string) {
        return this.fromEvent(event_name).map(
            response => response
        );
    }
}