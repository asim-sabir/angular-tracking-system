import {Injectable} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {CommonFunctions} from '../helpers/common.functions';
import {RequestUrls} from '../helpers/request.urls';
import {DataSharing} from '../helpers/data.sharing';

declare var $: any;

@Injectable()
export class LoginService {
    private _token: string;

    constructor(private common_functions: CommonFunctions, private request_urls: RequestUrls, private r_c: Router) {
    }

    CheckLogin(call_back) {
        this._token = DataSharing.retrieve('token');
        let login_url = this.request_urls.getUrl('auth.check_login');
        if (this._token) {
            this.common_functions.HttpGet(login_url).subscribe(response => {
                call_back(response);
            });
        } else {
            call_back(false);
        }
    }

    UserLogin(data, route) {
        this.common_functions.setLoading(true);
        let login_url = this.request_urls.getUrl('auth.login');
        this.common_functions.HttpPost(login_url, data).subscribe(response => {
            if (response.isResponse) {
                this.common_functions.setLoading(false);
                DataSharing.store('token', response.data.token);
                DataSharing.StoreJsonObj('info', response.data.info);
                route = DataSharing.GetLandingUrl(response.data.info);
                this.r_c.navigate([route]);
            } else {
                this.common_functions.setLoading(false);
                this.common_functions.ShowMessage(response.message, 'danger');
            }

        });
        return '';
    }
}
