import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
    {path: 'login', loadChildren: './pages/login/login.module#LoginModule'},
    {path: '', redirectTo: 'login', pathMatch: 'full'},
    {path: 'level-1', redirectTo: 'level-1/dashboard', pathMatch: 'full'},
    {path: 'level-2', redirectTo: 'level-2/index', pathMatch: 'full'},
    {path: 'level-3', redirectTo: 'level-3/index', pathMatch: 'full'},
    // {path: '**', loadChildren: './pages/login/login.module#LoginModule'},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}