import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {HttpModule} from '@angular/http';
import {AppRoutingModule} from './app-routing.module';
import {CommonFunctions} from './helpers/common.functions';
import {DataSharing} from './helpers/data.sharing';
import {BaseUrls} from './helpers/base.urls';
import {RequestUrls} from './helpers/request.urls';
import {LoginService} from './services/login.service';
import {AuthGuard} from './_guards/auth.guard';
import {RouteGuard} from './_guards/route.guard';
import {PermissionsGuard} from './_guards/permissions.guard';
import {SuperAdminComponent} from './pages/super-admin/super-admin.component';
import {SuperAdminRoutingModule} from './pages/super-admin/super-admin-routing.module';
import {Level2Component} from './pages/lavel-2/level-2.component';
import {Level2RoutingModule} from './pages/lavel-2/level-2-routing.module';
import {Level3Component} from './pages/level-3/level-3.component';
import {Level3RoutingModule} from './pages/level-3/level-3-routing.module';
import {SharedModule} from './shared/shared.module';
import {Emitters} from './helpers/emitters/emitters';
import {NotificationEmitter} from './helpers/emitters/notification.emitter';
import {LayoutSharedModule} from './shared/layout.share.module';
import { PushNotificationModule } from 'ng-push-notification';

@NgModule({
    declarations: [
        AppComponent,
        SuperAdminComponent,
        Level2Component,
        Level3Component,
    ],
    imports: [
        SharedModule,
        LayoutSharedModule,
        BrowserModule,
        AppRoutingModule,
        HttpModule,
        SuperAdminRoutingModule,
        Level2RoutingModule,
        Level3RoutingModule,
        PushNotificationModule.forRoot(/* Default settings here, interface PushNotificationSettings */),
    ],
    providers: [
        CommonFunctions,
        Emitters,
        NotificationEmitter,
        DataSharing,
        BaseUrls,
        RequestUrls,
        LoginService,
        RouteGuard,
        AuthGuard,
        PermissionsGuard
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
