﻿import {Helper} from './helper';

declare var $: any;

export class TicketStatusList {

    static List() {
        return {
            // level 3 routes
            ticket: {
                status: {
                    0: {
                        val: 'Open',
                        color: 'warning'
                    },
                    1: {
                        val: 'Assigned',
                        color: 'primary'
                    },
                    2: {
                        val: 'Reject',
                        color: 'success'
                    },
                    3: {
                        val: 'Close',
                        color: 'info'
                    }
                }
            },
        }
    }

    static getStatus(key) {
        return Helper.DecodeData(key, TicketStatusList.List());
    }

    static GetPlainStatusList(key, remove_index = -1) {
        let array = [];
        let split_key = key.split('.');
        let list = TicketStatusList.List();
        let obj = list[split_key[0]];
        if (split_key.length > 1) {
            obj = list[split_key[0]][split_key[1]];
        }
        $.each(obj, function (k, v) {
            array.push({
                id: k,
                name: v.val,
                color: v.color,
            });
        });
        if (remove_index >= 0) {
            array.splice(remove_index, 1);
        }
        return array;
    }
}
