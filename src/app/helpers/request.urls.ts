﻿import {Injectable} from '@angular/core';
import {BaseUrls} from './base.urls';
import {ApiUrls} from './routes/api.urls';

@Injectable()
export class RequestUrls {
    private base_url = '';
    private urls;

    constructor(private BaseUrls: BaseUrls) {
        this.base_url = this.BaseUrls.getUrl();
        this.urls = ApiUrls.GetApiUrl();
    }

    getUrl(key) {
        let split_key = key.split('.');
        if (split_key.length > 1) {
            let temp = null;
            let temp_path = '';
            let path = '';
            for (let i = 0; i < split_key.length; i++) {
                if (typeof temp === 'object' && temp !== null) {
                    temp = temp[split_key[i]];
                    temp_path = split_key[i];
                    if (i !== (split_key.length - 1)) {
                        temp_path += '/';
                    }
                }
                if (typeof temp === 'string') {
                    temp_path = temp;
                }
                if (temp === null) {
                    temp = this.urls[split_key[i]];
                    temp_path = split_key[i] + '/';
                }
                path += temp_path;
            }
            return this.base_url + path;
        }
        return this.base_url + this.urls[key];
    }

    getBaseUrl() {
        return this.BaseUrls.getBaseUrl();
    }
}
