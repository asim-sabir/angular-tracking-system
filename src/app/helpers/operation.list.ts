﻿import {Helper} from './helper';

declare var $: any;

export class OperationList {

    static List() {
        return {
            // operational ticket
            operational: {
                ticket: {
                    0: {
                        val: 'Edit client basic info',
                        color: 'warning'
                    },
                    1: {
                        val: 'Edit client e-contact info',
                        color: 'primary'
                    },
                    2: {
                        val: 'Edit vehicle info',
                        color: 'success'
                    },
                    3: {
                        val: 'Change of vehicle',
                        color: 'info'
                    },
                    4: {
                        val: 'Remove/De install',
                        color: 'info'
                    },
                    5: {
                        val: 'Change of confidential info',
                        color: 'info'
                    },
                    6: {
                        val: 'Change of plan',
                        color: 'info'
                    }
                }
            },
        }
    }

    static getOperation(key) {
        return Helper.DecodeData(key, OperationList.List());
    }

    static GetPlainOperationList(key, remove_index = -1) {
        let array = [];
        let split_key = key.split('.');
        let list = OperationList.List();
        let obj = list[split_key[0]];
        if (split_key.length > 1) {
            obj = list[split_key[0]][split_key[1]];
        }
        $.each(obj, function (k, v) {
            array.push({
                id: k,
                name: v.val,
                color: v.color,
            });
        });
        if (remove_index >= 0) {
            array.splice(remove_index, 1);
        }
        return array;
    }
}
