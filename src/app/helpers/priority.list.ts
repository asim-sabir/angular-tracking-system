﻿import {Helper} from './helper';

declare var $: any;

export class PriorityList {

    static List() {
        return {
            // ticket priority
            ticket: {
                priority: {
                    0: {
                        val: 'Normal',
                        color: 'warning'
                    },
                    1: {
                        val: 'Minor',
                        color: 'primary'
                    },
                    2: {
                        val: 'Major',
                        color: 'success'
                    },
                    3: {
                        val: 'Critical',
                        color: 'info'
                    }
                }
            },
        }
    }

    static getPriority(key) {
        return Helper.DecodeData(key, PriorityList.List());
    }

    static GetPlainPriorityList(key, remove_index = -1) {
        let array = [];
        let split_key = key.split('.');
        let list = PriorityList.List();
        let obj = list[split_key[0]];
        if (split_key.length > 1) {
            obj = list[split_key[0]][split_key[1]];
        }
        $.each(obj, function (k, v) {
            array.push({
                id: k,
                name: v.val,
                color: v.color,
            });
        });
        if (remove_index >= 0) {
            array.splice(remove_index, 1);
        }
        return array;
    }
}
