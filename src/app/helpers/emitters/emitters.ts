﻿import {Output, EventEmitter} from '@angular/core';

export class Emitters {
    @Output() ProfileChange: EventEmitter<any> = new EventEmitter();
    change(data) {
        this.ProfileChange.emit(data);
    }

    getEmittedValue() {
        return this.ProfileChange;
    }
}
