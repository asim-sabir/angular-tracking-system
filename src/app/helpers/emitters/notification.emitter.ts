﻿import {Output, EventEmitter} from '@angular/core';

export class NotificationEmitter {
    @Output() Notifications: EventEmitter<any> = new EventEmitter();
    change(data) {
        this.Notifications.emit(data);
    }

    getEmittedValue() {
        return this.Notifications;
    }
}
