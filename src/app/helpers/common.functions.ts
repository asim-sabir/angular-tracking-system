﻿import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {Headers} from '@angular/http';
import {DataSharing} from './data.sharing';

declare let $: any;
declare let toastr: any;

@Injectable()
export class CommonFunctions {
    _headers;
    _content_types = {
        json: 'application/json',
        undefined: undefined,
        form: 'application/x-www-form-urlencoded; charset=utf-8',
    };
    _setting = {
        theme: '',
        sticky: false,
        horizontalEdge: '',
        verticalEdge: '',
        heading: '',
        life: 10000,
    }

    constructor(private http: Http) {
    }

    HttpGet(url, content_type = 'form') {
        return this.http.get(url, {headers: this.SetHeader(content_type)}).map(
            response => response.text() ? response.json() : {}
        );
    }

    HttpPost(url, data, content_type = 'form') {
        return this.http.post(url, data, {headers: this.SetHeader(content_type)}).map(
            response => response.json()
        );
    }

    HttpPut(url, data, content_type = 'form') {
        return this.http.put(url, data, {headers: this.SetHeader(content_type)}).map(
            response => response.json()
        );
    }

    HttpPatch(url, data, content_type = 'form') {
        return this.http.patch(url, data, {headers: this.SetHeader(content_type)}).map(
            response => response.json()
        );
    }

    HttpDelete(url, content_type = 'form') {
        return this.http.delete(url, {headers: this.SetHeader(content_type)}).map(
            response => response.json()
        );
    }

    FileUpload(url, data) {
        this._headers = new Headers({'Authorization': 'Bearer ' + DataSharing.retrieve('token')});
        return this.http.post(url, data, {headers: this._headers}).map(
            response => response.json()
        );
    }

    GetContentType(type = 'form') {
        return this._content_types[type];
    }

    SetHeader(content_type) {
        const headers = {
            'Content-Type': this.GetContentType(content_type),
            'Authorization': 'Bearer ' + DataSharing.retrieve('token')
        };
        return new Headers(headers);
    }

    ErrorMessages(error_data) {
        $('.required').html('');
        let first_key = null;
        $.each(error_data, function (key, val) {
            if (!first_key) {
                first_key = key;
            }
            $('#' + key).parent('div').find('.required').html(val);
        });
        $('html, body').animate({'scrollTop': $('#' + first_key).scrollTop() + 100});
    }

    /**
     * Themes [success,info,warning,error]
     * positionClass [toast-top-right,toast-bottom-right]
     * vertical-position [right,left]
     * life [1000, 5000, 10000, 25000, 60000]
     */
    ShowToast(message, theme = 'success', title = '', positionClass = 'toast-top-right') {
        toastr.options = {
            closeButton: false,
            debug: false,
            newestOnTop: false,
            progressBar: false,
            positionClass: positionClass,
            preventDuplicates: false,
            onclick: null,
            showDuration: 300,
            hideDuration: 1000,
        };
        toastr[theme](message, title);
    }

    setLoading(enable) {
        let body = $('body');
        if (enable) {
            $(body).addClass('m-page--loading-non-block');
        } else {
            $(body).removeClass('m-page--loading-non-block');
        }
    }

    createGroupedArray(arr, chunkSize) {
        const groups = [];
        let i;
        for (i = 0; i < arr.length; i += chunkSize) {
            groups.push(arr.slice(i, i + chunkSize));
        }
        return groups;
    }

    ShowMessage(Message, type) {
        $('#alert-message').html(this.MessageAlerts(Message, type));
    }

    MessageAlerts(Message, type) {
        return '<div role="alert" class="m-alert m-alert--outline alert alert-' + type + ' alert-dismissible">\n' +
            ' <button aria-label="Close" class="close" data-dismiss="alert" type="button"></button>\n' +
            ' <span>' + Message + '</span>\n' +
            '</div>';
    }

    OpenModel(id) {
        $('#' + id).modal('show');
    }

    CloseModel(id) {
        $('#' + id).modal('hide');
    }
}
