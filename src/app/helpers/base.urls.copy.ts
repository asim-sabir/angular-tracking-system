﻿export class BaseUrls {
    /*-- localhost url --*/
    // private base_url = 'http://tracking-system.app/';
    /*-- dev server url --*/
    private base_url = 'http://connexispanel.alpharages.com/';
    private prefix = 'api/bMK38z7WFfXR/';

    getUrl() {
        return this.base_url + this.prefix;
    }

    getBaseUrl() {
        return this.base_url;
    }
}
