﻿export class ApiUrls {
    static urls = {
        auth: {
            login: 'login',
            check_login: 'check',
            permission: 'permissions',
            logout: 'logout'
        },
        /**-- level 1 routes --**/
        level1: {
            company: {
                index: 'list',
                add: 'add',
                info: 'info',
                update: 'update',
                destroy: 'destroy',
            },
            product: {
                index: 'list',
                post: 'add',
                get: 'info',
                view: 'view',
                put: 'update',
                delete: 'destroy',
            },
            feature: {
                index: 'list',
                post: 'add',
                get: 'info',
                put: 'update',
                delete: 'destroy',
            },
            package: {
                index: 'list',
                get_add: 'get-add',
                post: 'add',
                get: 'info',
                get_update: 'get-update',
                put: 'update',
                delete: 'destroy',
            },
            user: {
                index: 'list',
                get_add: 'get-add',
                post: 'add',
                get: 'info',
                get_update: 'get-update',
                put: 'update',
                delete: 'destroy',
            },
            role: {
                index: 'list',
                get_add: 'get-add',
                post: 'add',
                get: 'info',
                get_update: 'get-update',
                put: 'update',
                delete: 'destroy',
            },
        },
        /**-- level 2 routes --**/
        level2: {
            hod: {
                /**-- sale routes --**/
                sale: {
                    header: {
                        get: 'info'
                    },
                    dashboard: {
                        get: 'info'
                    },
                    user: {
                        get: 'info',
                        put: 'update'
                    },
                    agent: {
                        index: 'list',
                        get: 'info'
                    },
                    client: {
                        index: 'list',
                        get_add: 'get-add',
                        post: 'add',
                        get: 'info',
                        get_update: 'get-update',
                        put: 'update',
                        delete: 'destroy',
                        assign_agent: 'assign-agent',
                        remove_agent: 'remove-agent',
                        followup: {
                            post: 'add',
                        }
                    },
                    proposal: {
                        index: 'list',
                        get_add: 'get-add',
                        post: 'add',
                        get: 'info',
                        delete: 'destroy',
                    },
                    lead: {
                        index: 'list',
                        get_add: 'get-add',
                        post: 'add',
                        get: 'info',
                        status_put: 'status-update',
                        assign_agent: 'assign-agent',
                        remove_agent: 'remove-agent',
                        followup: {
                            post: 'add',
                        }
                    },
                    sale: {
                        index: 'list',
                        get: 'info'
                    },
                    report: {
                        get: 'info'
                    },
                    ticket: {
                        index: 'list',
                        get_add: 'get-add',
                        post: 'add',
                        get: 'info',
                        status_put: 'status-update',
                        update_priority: 'priority-update',
                        update_status: 'status-update',
                        assign_agent: 'assign-agent',
                        remove_agent: 'remove-agent',
                        assign_dept: 'assign-dept',
                        followup: {
                            post: 'add',
                        }
                    },
                },
                /**-- crm routes --**/
                crm: {
                    // user route
                    header: {
                        get: 'info'
                    },
                    // dashboard route
                    dashboard: {
                        get: 'info'
                    },
                    // user routes
                    user: {
                        get: 'info',
                        put: 'update'
                    },
                    agent: {
                        index: 'list',
                        get: 'info'
                    },
                    // client routes
                    client: {
                        index: 'list',
                        get: 'info',
                        get_update: 'get-update',
                        put: 'update',
                        followup: {
                            post: 'add',
                        },
                        emergency_contact: {
                            get_update: 'get-update',
                            put: 'update',
                        },
                        vehicle: {
                            get_update: 'get-update',
                            put: 'update',
                        }
                    },
                    lead: {
                        index: 'list',
                        get_agent: 'get-agent',
                        get_update: 'get-update',
                        put: 'update',
                        assign_agent: 'assign-agent',
                        remove_agent: 'remove-agent',
                        followup: {
                            post: 'add',
                        }
                    },
                    sale: {
                        index: 'list',
                        get: 'info'
                    },
                    report: {
                        get: 'info'
                    },
                    ticket: {
                        index: 'list',
                        get_add: 'get-add',
                        post: 'add',
                        get: 'info',
                        status_put: 'status-update',
                        update_priority: 'priority-update',
                        update_status: 'status-update',
                        assign_agent: 'assign-agent',
                        remove_agent: 'remove-agent',
                        assign_dept: 'assign-dept',
                        followup: {
                            post: 'add',
                        },
                        operational: {
                            index: 'list',
                            get_add: 'get-add',
                            post: 'add',
                            get: 'info',
                            status_put: 'status-update',
                            update_priority: 'priority-update',
                            update_status: 'status-update',
                            assign_agent: 'assign-agent',
                            remove_agent: 'remove-agent',
                            assign_dept: 'assign-dept',
                        }
                    },
                }
            }
        },
        /**-- level 3 routes --**/
        level3: {
            /**-- sale routes --**/
            sale: {
                header: {
                    get: 'info'
                },
                dashboard: {
                    get: 'info'
                },
                user: {
                    get: 'info',
                    put: 'update'
                },
                client: {
                    index: 'list',
                    get_add: 'get-add',
                    post: 'add',
                    get: 'info',
                    get_update: 'get-update',
                    put: 'update',
                    delete: 'destroy',
                    followup: {
                        post: 'add',
                    }
                },
                proposal: {
                    index: 'list',
                    get_add: 'get-add',
                    post: 'add',
                    get: 'info',
                    delete: 'destroy',
                },
                lead: {
                    index: 'list',
                    get_add: 'get-add',
                    post: 'add',
                    get: 'info',
                    followup: {
                        post: 'add',
                    }
                },
                sale: {
                    index: 'list',
                    get: 'info'
                },
                report: {
                    get: 'info'
                },
                ticket: {
                    index: 'list',
                    get_add: 'get-add',
                    post: 'add',
                    get: 'info',
                    status_put: 'status-update',
                    update_priority: 'priority-update',
                    update_status: 'status-update',
                    assign_agent: 'assign-agent',
                    remove_agent: 'remove-agent',
                    assign_dept: 'assign-dept',
                    followup: {
                        post: 'add',
                    },
                    operational: {
                        index: 'list',
                        get_add: 'get-add',
                        post: 'add',
                        get: 'info',
                        status_put: 'status-update',
                        update_priority: 'priority-update',
                        update_status: 'status-update',
                        assign_agent: 'assign-agent',
                        remove_agent: 'remove-agent',
                        assign_dept: 'assign-dept',
                    }
                }
            },
            /**-- crm routes --**/
            crm: {
                // user route
                header: {
                    get: 'info'
                },
                // dashboard route
                dashboard: {
                    get: 'info'
                },
                // user routes
                user: {
                    get: 'info',
                    put: 'update'
                },
                // client routes
                client: {
                    index: 'list',
                    get: 'info',
                    get_update: 'get-update',
                    put: 'update',
                    followup: {
                        post: 'add',
                    },
                    emergency_contact: {
                        get_update: 'get-update',
                        put: 'update',
                    },
                    vehicle: {
                        get_update: 'get-update',
                        put: 'update',
                    }
                },
                lead: {
                    index: 'list',
                    get_update: 'get-update',
                    put: 'update',
                    followup: {
                        post: 'add',
                    }
                },
                sale: {
                    index: 'list',
                    get: 'info'
                },
                report: {
                    get: 'info'
                },
                ticket: {
                    index: 'list',
                    get_add: 'get-add',
                    post: 'add',
                    get: 'info',
                    status_put: 'status-update',
                    update_priority: 'priority-update',
                    update_status: 'status-update',
                    assign_agent: 'assign-agent',
                    remove_agent: 'remove-agent',
                    assign_dept: 'assign-dept',
                    followup: {
                        post: 'add',
                    },
                    operational: {
                        index: 'list',
                        get_add: 'get-add',
                        post: 'add',
                        get: 'info',
                        status_put: 'status-update',
                        update_priority: 'priority-update',
                        update_status: 'status-update',
                        assign_agent: 'assign-agent',
                        remove_agent: 'remove-agent',
                        assign_dept: 'assign-dept',
                    }
                },
            }
        },
    };

    static GetApiUrl() {
        return ApiUrls.urls;
    }
}
