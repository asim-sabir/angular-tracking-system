﻿import {DataSharing} from './data.sharing';

export class Helper {

    static DecodeData(key, list) {
        let split_key = key.split('.');
        if (split_key.length > 1) {
            let temp = null;
            let value = null;
            for (let i = 0; i < split_key.length; i++) {
                if (typeof temp === 'object' && temp !== null) {
                    temp = temp[split_key[i]];
                }
                if (typeof temp === 'string') {
                    value = temp;
                }
                if (temp === null) {
                    temp = list[split_key[i]];
                }
            }
            return value;
        }
        return '';
    }

    static MakeFullPermssionString(data) {
        let info = DataSharing.RetriveJsonObj('info');
        return info.level + '_' + info.dept.toLowerCase() + '_' + data;
    }

    static CheckPermissionExist(permission) {
        let return_val = false;
        let user_permission = DataSharing.user_permissions();
        user_permission.forEach(function (val, key) {
            if (permission === val.name) {
                return_val = true;
                return true;
            }
        });
        return return_val;
    }
}
