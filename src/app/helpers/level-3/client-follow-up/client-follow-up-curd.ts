declare var $: any;

export class ClientFollowUpCurd {
    static ValidForm(obj) {
        let form = obj.closest('form');
        form.validate({
            rules: {
                commented_by: {
                    required: true
                },
                comment: {
                    required: true
                }
            }
        });
        if (!form.valid()) {
            return false;
        }
        return true;
    }
}