declare var $: any;

export class ClientCurd {
    static ValidForm(obj) {
        let form = obj.closest('form');
        form.validate({
            rules: {
                customer_name: {
                    required: true
                },
                company_name: {
                    required: true
                },
                city: {
                    required: true
                },
                country: {
                    required: true
                }
            }
        });
        if (!form.valid()) {
            return false;
        }
        return true;
    }

    static FormRepeater() {
        let self = this;
        $('#add_more_vehicle').on('click', function () {
            let clone_div = $('#vehicle_div .clone_div:first').clone();
            clone_div.insertAfter('#vehicle_div .clone_div:last');
            self.CalculateVehicle();
            self.RenameName();
        });
    }

    static DeleteFormRepeater() {
        let self = this;
        $(document).on('click', '.delete-vehicle', function () {
            if ($('.clone_div').length > 1) {
                $(this).parents('.clone_div').remove();
                self.CalculateVehicle();
                self.RenameName();
            }
        });
    }

    static CalculateVehicle() {
        $('.vehicle-counter').each(function (key, val) {
            $(this).html(parseInt(key) + 1);
        });
    }

    static RenameName() {
        $('.clone_div').each(function (key, val) {
            $(this).find('input').each(function () {
                var name = $(this).attr('data-name') + '[' + key + ']';
                $(this).attr('name', name);
            });
        });
    }

    static init() {
        this.FormRepeater();
        this.DeleteFormRepeater();
    }
}