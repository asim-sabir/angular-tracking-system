declare var $: any;

export class ProposalCurd {
    static ValidForm(obj) {
        let form = obj.closest('form');
        form.validate({
            rules: {
                name: {
                    required: true
                },
                customer_id: {
                    required: true
                },
                package_id: {
                    required: true
                }
            }
        });
        if (!form.valid()) {
            return false;
        }
        return true;
    }
}