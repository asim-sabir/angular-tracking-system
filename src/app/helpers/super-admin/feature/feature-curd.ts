declare var $: any;

export class FeatureCurd {
    static ValidForm(obj) {
        let form = obj.closest('form');
        form.validate({
            rules: {
                feature_name: {
                    required: true
                },
                feature_sale_price: {
                    required: true,
                    number: true
                },
                feature_cost_price: {
                    required: true,
                    number: true
                },
                feature_type: {
                    required: true
                }
            }
        });
        if (!form.valid()) {
            return false;
        }
        return true;
    }
}