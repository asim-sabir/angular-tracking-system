declare var $: any;

export class CompanyCurd {
    static ValidForm(obj) {
        let form = obj.closest('form');
        form.validate({
            rules: {
                name: {
                    required: true
                }
            }
        });
        if (!form.valid()) {
            return false;
        }
        return true;
    }
}