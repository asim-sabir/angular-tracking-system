declare var $: any;

export class ProductCurd {
    static ValidForm(obj) {
        let form = obj.closest('form');
        form.validate({
            rules: {
                product_name: {
                    required: true
                },
                product_model: {
                    required: true
                },
                company_id: {
                    required: true
                }
            }
        });
        if (!form.valid()) {
            return false;
        }
        return true;
    }
}