declare var $: any;

export class PlanCurd {
    static ValidForm(obj) {
        let form = obj.closest('form');
        form.validate({
            rules: {
                package_name: {
                    required: true
                },
                product_id: {
                    required: true
                },
                package_min_cost: {
                    required: true,
                    number: true
                },
                package_sale_price: {
                    required: true,
                    number: true
                },
                package_discount: {
                    number: true
                }
            }
        });
        if (!form.valid()) {
            return false;
        }
        return true;
    }

    static FormRepeater() {
        $('#add_more').on('click', function () {
            let clone_div = $('.features_div .clone_div:first').clone();
            clone_div.insertAfter('.features_div .clone_div:last');
            PlanCurd.TotalPrice();
        });
    }

    static DeleteFormRepeater() {
        $(document).on('click', '.delete-item', function () {
            if ($('.clone_div').length > 1) {
                $(this).parents('.clone_div').remove();
                PlanCurd.TotalPrice();
            }
        });
    }

    static ChangeFeature() {
        $(document).on('change', '.feature_select', function () {
            PlanCurd.TotalPrice();
        });
    }

    static FreeFeature() {
        $(document).on('click', '.is_free', function () {
            PlanCurd.TotalPrice();
        });
    }

    static Discount() {
        $('#package_discount').keyup(function () {
            PlanCurd.TotalPrice();
        });
    }

    static CalculatePrice() {
        let price = 0;
        $('.clone_div').each(function () {
            if (!$(this).find('.is_free').is(':checked')) {
                price = price + (parseFloat($(this).find('.feature_select option:selected').attr('data-price')));
            }
        });
        return price;
    }

    static CalculateDiscount() {
        let discount = 0;
        if ($('#package_discount').val()) {
            discount = parseFloat($('#package_discount').val());
        }
        return discount;
    }

    static TotalPrice() {
        let price = this.CalculatePrice();
        let discount = this.CalculateDiscount();
        let total_price = price - discount;
        if (total_price >= 0) {
            $('#total-price').html(price);
            $('#sale-price').val(total_price);
        }
    }

    static init() {
        this.FormRepeater();
        this.DeleteFormRepeater();
        this.ChangeFeature();
        this.FreeFeature();
        this.Discount();
    }
}