import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {Socket} from 'ng-socket-io';
import {DataSharing} from '../data.sharing';

@Injectable()
export class NotificationSocket extends Socket {
    constructor() {
        super({url: 'http://http://127.0.0.1:3000', options: {}});
    }
}