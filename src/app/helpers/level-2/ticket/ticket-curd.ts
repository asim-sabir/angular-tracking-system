declare var $: any;

export class TicketCurd {
    static ValidForm(obj) {
        let form = obj.closest('form');
        form.validate({
            rules: {
                priority_level: {
                    required: true
                },
                customer_id: {
                    required: true
                },
                dept_id: {
                    required: true
                },
                ticket_title: {
                    required: true
                },
                description: {
                    required: true
                }
            }
        });
        if (!form.valid()) {
            return false;
        }
        return true;
    }
}