declare var $: any;

export class ClientCurd {
    static ValidForm(obj) {
        let form = obj.closest('form');
        form.validate({
            rules: {
                customer_name: {
                    required: true
                },
                company_name: {
                    required: true
                },
                city: {
                    required: true
                },
                country: {
                    required: true
                }
            }
        });
        if (!form.valid()) {
            return false;
        }
        return true;
    }
}