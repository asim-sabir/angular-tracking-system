﻿export class DataSharing {
    data = [];

    static store(key, val) {
        return localStorage.setItem(key, val);
    }

    static retrieve(key) {
        return localStorage.getItem(key);
    }

    static user_dept_id() {
        let info = DataSharing.GetJsonObj('info');
        return info.dept_id;
    }
    static user_dept() {
        let info = DataSharing.GetJsonObj('info');
        return info.dept.toLowerCase();
    }

    static user_post() {
        let info = DataSharing.GetJsonObj('info');
        if (info.post)
            return info.post.toLowerCase();
        return 'manager';
    }

    static user_role() {
        let info = DataSharing.GetJsonObj('info');
        return info.roles[0].name;
    }

    static user_permissions() {
        let info = DataSharing.GetJsonObj('info');
        return info.permissions;
    }

    static remove(key) {
        return localStorage.removeItem(key);
    }

    static clearAll() {
        return localStorage.clear();
    }

    static StoreJsonObj(key, data) {
        return DataSharing.store(key, JSON.stringify(data));
    }

    static GetJsonObj(key) {
        return JSON.parse(DataSharing.retrieve(key));
    }

    static GetLandingUrl(data) {
        let dept = DataSharing.user_dept();
        let post = DataSharing.user_post();
        if (data.level === 'level-1') {
            if (data.roles[0].name === 'Admin') {
                return '/level-1';
            } else {
                return '/level-1/' + dept + '/index';
            }
        } else if (data.level === 'level-2') {
            return '/level-2/' + post + '/' + dept + '/index';
        } else if (data.level === 'level-3') {
            return '/level-3/' + dept + '/index';
        }
        return '/404';
    }

    static RetriveJsonObj(key) {
        return JSON.parse(DataSharing.retrieve(key));
    }

    temp_store(key, val) {
        return this.data[key] = val;
    }

    temp_retrieve(key) {
        return this.data[key];
    }

}
