﻿import {PermissionsData} from './permissions/permissions.data';
import {Helper} from './helper';

export class Permissions {
    static BreakRoute(route, route_info) {
        // check route params first
        let id = Permissions.CHeckRouteParams(route_info);
        let split_route = route.split('/');
        let new_route = '';
        split_route.forEach(function (val, key,) {
            if (val) {
                if (val != id) {
                    new_route += val + '.';
                }
            }
        });
        // remove last dot
        new_route = new_route.replace(/.\s*$/, '');
        return new_route;
    }

    static CheckRoute(route_data, route_info) {
        let route = Permissions.BreakRoute(route_data, route_info);
        return Helper.CheckPermissionExist(PermissionsData.GetPermissions(route));
    }

    static CHeckRouteParams(route) {
        for (let val in route.params) {
            if (val) {
                return route.paramMap.params[val];
            }
        }
        return false;
    }
}
