﻿import {Helper} from '../helper';

export class PermissionsData {
    static permission =
        {
            /**-- level 2 permissions --**/
            'level-2': {
                /**-- manager --*/
                hod: {
                    // sale
                    sale: {
                        index: 'level-2_hod_sale_index_view',
                        clients: 'level-2_hod_sale_client_list',
                        client: {
                            new: {
                                individual: 'level-2_hod_sale_client_add',
                                corporate: 'level-2_hod_sale_client_add'
                            },
                            update: {
                                individual: 'level-2_hod_sale_client_update',
                                corporate: 'level-2_hod_sale_client_update'
                            },
                            view: 'level-2_hod_sale_client_view'
                        },
                        proposals: 'level-2_hod_sale_proposal_list',
                        proposal: {
                            new: 'level-2_hod_sale_proposal_add'
                        },
                        leads: 'level-2_hod_sale_lead_list',
                        lead: {
                            new: 'level-2_hod_sale_lead_add',
                            view: 'level-2_hod_sale_lead_view'
                        },
                        sales: 'level-2_hod_sale_sale_list',
                        reports: 'level-2_hod_sale_report_view',
                        profile: 'level-2_hod_sale_user_profile_view',
                        agents: 'level-2_hod_sale_agent_list',
                        agent: {
                            view: 'level-2_hod_sale_agent_view'
                        },
                        tickets: 'level-2_hod_sale_ticket_list',
                        ticket: {
                            new: 'level-2_hod_sale_ticket_add',
                            view: 'level-2_hod_sale_ticket_view'
                        },
                    },
                    // crm
                    crm: {
                        index: 'level-2_hod_crm_index_view',
                        profile: 'level-2_hod_crm_user_profile_view',
                        clients: 'level-2_hod_crm_client_list',
                        client: {
                            update: {
                                individual: 'level-2_hod_crm_approved_client_update',
                                corporate: 'level-2_hod_crm_approved_client_update',
                                'emergency-contact': 'level-2_hod_crm_approved_emergency_contact_update',
                                vehicle: 'level-2_hod_crm_approved_vehicle_update',
                            },
                            view: 'level-2_hod_crm_client_view'
                        },
                        leads: 'level-2_hod_crm_lead_list',
                        lead: {
                            update: 'level-2_hod_crm_lead_update'
                        },
                        reports: 'level-2_hod_sale_report_view',
                        tickets: 'level-2_hod_crm_ticket_list',
                        ticket: {
                            new: 'level-2_hod_crm_ticket_add',
                            view: 'level-2_hod_crm_ticket_view'
                        },
                        operational: {
                            tickets: 'level-2_hod_crm_operational_ticket_list',
                            ticket: {
                                new: 'level-2_hod_crm_operational_ticket_add',
                                view: 'level-2_hod_crm_operational_ticket_view',
                            }
                        },
                        agents: 'level-2_hod_crm_agent_list',
                        agent: {
                            view: 'level-2_hod_crm_agent_view'
                        },
                    }
                },
            },
            /**-- level 3 permissions --**/
            'level-3': {
                // sale
                sale: {
                    index: 'level-3_sale_index_view',
                    profile: 'level-3_sale_user_profile_view',
                    clients: 'level-3_sale_client_list',
                    client: {
                        new: {
                            individual: 'level-3_sale_client_add',
                            corporate: 'level-3_sale_client_add'
                        },
                        update: {
                            individual: 'level-3_sale_client_update',
                            corporate: 'level-3_sale_client_update'
                        },
                        view: 'level-3_sale_client_view'
                    },
                    proposals: 'level-3_sale_proposal_list',
                    proposal: {
                        new: 'level-3_sale_proposal_add'
                    },
                    leads: 'level-3_sale_lead_list',
                    lead: {
                        new: 'level-3_sale_lead_add',
                        view: 'level-3_sale_lead_view',
                    },
                    sales: 'level-3_sale_sale_list',
                    reports: 'level-3_sale_report_view',
                    tickets: 'level-3_sale_ticket_list',
                    ticket: {
                        new: 'level-3_sale_ticket_add',
                        view: 'level-3_sale_ticket_view'
                    }
                },
                // crm
                crm: {
                    index: 'level-3_crm_index_view',
                    profile: 'level-3_crm_user_profile_view',
                    clients: 'level-3_crm_client_list',
                    client: {
                        update: {
                            individual: 'level-3_crm_approved_client_update',
                            corporate: 'level-3_crm_approved_client_update',
                            'emergency-contact': 'level-3_crm_approved_emergency_contact_update',
                            vehicle: 'level-3_crm_approved_vehicle_update',
                        },
                        view: 'level-3_crm_client_view'
                    },
                    leads: 'level-3_crm_lead_list',
                    lead: {
                        update: 'level-3_crm_lead_view'
                    },
                    reports: 'level-3_sale_report_view',
                    tickets: 'level-3_crm_ticket_list',
                    ticket: {
                        new: 'level-3_crm_ticket_add',
                        view: 'level-3_crm_ticket_view'
                    },
                    operational: {
                        tickets: 'level-3_crm_operational_ticket_list',
                        ticket: {
                            new: 'level-3_crm_operational_ticket_add',
                            view: 'level-3_crm_operational_ticket_view',
                        }
                    },
                }
            }
        };

    static GetPermissions(key) {
        return Helper.DecodeData(key, PermissionsData.permission);
    }
}
