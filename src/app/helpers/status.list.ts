﻿import {Helper} from './helper';

declare var $: any;

export class StatusList {

    static List() {
        return {
            // level 3 routes
            level3: {
                lead: {
                    0: {
                        val: 'In progress',
                        color: 'warning'
                    },
                    1: {
                        val: 'Approval request',
                        color: 'primary'
                    },
                    2: {
                        val: 'Approved',
                        color: 'success'
                    },
                    /*3: {
                        val: 'Revision',
                        color: 'info'
                    },*/
                    4: {
                        val: 'Rejected',
                        color: 'danger'
                    },
                    5: {
                        val: 'Cancel',
                        color: 'secondary'
                    }
                }
            },
        }
    }

    static getStatus(key) {
        return Helper.DecodeData(key, StatusList.List());
    }

    static GetPlainStatusList(key, remove_index = -1) {
        let array = [];
        let split_key = key.split('.');
        let list = StatusList.List();
        let obj = list[split_key[0]];
        if (split_key.length > 1) {
            obj = list[split_key[0]][split_key[1]];
        }
        $.each(obj, function (k, v) {
            array.push({
                id: k,
                name: v.val,
                color: v.color,
            });
        });
        if (remove_index >= 0) {
            array.splice(remove_index, 1);
        }
        return array;
    }
}
