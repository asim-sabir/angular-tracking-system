import {NgModule} from '@angular/core';
import {SuperAdminLayoutModule} from '../layouts/super-admin/super-admin-layout.module';
import {Level2LayoutModule} from '../layouts/level-2/level-2-layout.module';
import {Level3LayoutModule} from '../layouts/level-3/level-3-layout.module';

@NgModule({
    declarations: [
    ],
    exports: [
        SuperAdminLayoutModule,
        Level2LayoutModule,
        Level3LayoutModule,
    ],
    providers: []
})
export class LayoutSharedModule {
}
