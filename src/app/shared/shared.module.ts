import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {FilterPipe} from '../pipe/filter.pipe';
import {SelectorTwoDirective} from '../directives/selector-two.directive';
import {DatePickerDirective} from '../directives/date-picker.directive';
import {NgxPaginationModule} from 'ngx-pagination';
import {SocketIoModule} from 'ng-socket-io';
import {NotificationService} from '../services/socket/notification.service';
// import {CheckLayout3PermissionsDirective} from "../directives/permissions/check-layout3-permissions.directive";

@NgModule({
    declarations: [
        SelectorTwoDirective,
        DatePickerDirective,
        // CheckLayout3PermissionsDirective,
        FilterPipe
    ],
    imports: [
        SocketIoModule
    ],
    exports: [
        FormsModule,
        SelectorTwoDirective,
        DatePickerDirective,
        // CheckLayout3PermissionsDirective,
        FilterPipe,
        NgxPaginationModule
    ],
    providers: [
        NotificationService
    ]
})
export class SharedModule {
}
